<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:30
 */

if (!isset($nom)) {
    $nom = "";
}

if (!isset($prenom)) {
    $prenom = "";
}

?>

<!doctype html>
<html lang="fr">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/css/animate.min.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/css/fontawesome/css/fontawesome-all.min.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/css/bootstrap-datetimepicker.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/css/bootstrap-timepicker.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/datatables.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/AutoFill-2.2.2/css/autoFill.bootstrap4.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/Buttons-1.5.1/css/buttons.bootstrap4.css">

        <link rel="stylesheet" type="text/css"
        href="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/DataTables-1.10.16/css/dataTables.bootstrap4.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/css/yeti.css">

        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['BaseURL'] ?>assets/css/bootstrap-switch.css">

        <title>Ecogardes</title>

        <link rel="icon" type="image/png" href="<?= $GLOBALS['BaseURL'] ?>assets/img/logo.ico"/>
    </head>

    <body>

        <header>
            <div class="navbar navbar-dark bg-dark box-shadow">
                <div class="container d-flex justify-content-between">
                    <a href="#" class="navbar-brand d-flex align-items-center">
                        <i class="fas fa-leaf"></i>
                        <strong>Ecogardes</strong>
                    </a>
                    <a class="navbar-brand form-inline my-2 my-lg-0" style="color: white;">
                        <?php
                        if ($nom !== "" && $prenom !== "") {
                            echo "Bonjour $nom $prenom <a href='".$router->generateUri("login.get.logout", [])."' class=\"btn btn-outline-danger my-2 my-sm-0\" style='color: white;'>Déconnexion <i class=\"fas fa-sign-out-alt\"></i></a>";
                        }
                        ?>
                    </a>
                </div>
            </div>
        </header>