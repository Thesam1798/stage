<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:40
 */
?>

<footer class="text-muted">

</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/bootstrap.min.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/datatables.js"></script>

<script type="text/javascript" charset="utf8"
        src="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/AutoFill-2.2.2/js/autoFill.bootstrap4.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/Buttons-1.5.1/js/buttons.bootstrap4.js"></script>

<script type="text/javascript" charset="utf8"
        src="<?= $GLOBALS['BaseURL'] ?>assets/DataTables/DataTables-1.10.16/js/dataTables.bootstrap4.js"></script>


<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/popper.min.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/moment.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/moment-with-locales.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/bootstrap-timepicker.js"></script>

<script type="text/javascript" charset="utf8" src="<?= $GLOBALS['BaseURL'] ?>assets/js/bootstrap-switch.js"></script>

<script type="text/javascript">
    moment.locale('fr');

    $(document).ready(function () {

        var config = {
            "language": {
                "sProcessing":     "Traitement en cours...",
                "sSearch":         "Rechercher&nbsp;:",
                "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix":    "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst":      "Premier",
                    "sPrevious":   "Pr&eacute;c&eacute;dent",
                    "sNext":       "Suivant",
                    "sLast":       "Dernier"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            }
        };

        $('#tables_MML').DataTable(config);
        $('#tables_BI').DataTable(config);
    });

</script>

</body>
</html>
