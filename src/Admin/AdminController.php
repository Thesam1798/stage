<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 05/06/18
 * Time: 13:42
 */

namespace App\Admin;

use App\Admin\SubController\Extract;
use App\Admin\SubController\GetMission;
use App\Utilisateur\SubController\AddPlanning;
use App\Utilisateur\SubController\Index;
use finfo;
use Framework\Config;
use Framework\Database;
use Framework\Renderer;
use Framework\Router;
use function GuzzleHttp\Psr7\_caseless_remove;
use GuzzleHttp\Psr7\ServerRequest;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class AdminController
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * AdminController constructor.
     * @param Renderer $renderer
     * @param Router $router
     */
    public function __construct(Renderer $renderer, Router $router)
    {
        $this->renderer = $renderer;
        $this->router = $router;
    }

    /**
     * Apelle de l'index
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_index(ServerRequest $request): string
    {
        return $this->renderer->render('@admin/index', []);
    }

    /**
     * apelle d'ajour d'utilisateur
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_add(ServerRequest $request): string
    {
        return $this->renderer->render('@admin/add', []);
    }

    /**
     * generation de la liste des misison
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_planning(ServerRequest $request): string
    {
        // Initialisation des valeur vide
        $MML = "";

        // Apelle a la DB
        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('mission'), [], true);

        // Liste des ecogarde / binome
        $binomeListe = [];

        foreach ($retour as $value) {

            // liste les binome et les ajoute dans $user_liste
            if ($value['binome'] !== -1) {
                if (!isset($binomeListe[$value['binome']])) {
                    $info = $database->SQL("SELECT nom, prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$value['binome']], true);
                    $binome = $info[0]['nom']." ".$info[0]['prenom'];
                    $binomeListe[$value['binome']] = $binome;
                } else {
                    $binome = $binomeListe[$value['binome']];
                }
            } else {
                $binome = "Seul";
            }

            // liste les ecogarde et les ajoute dans $user_liste
            if ($value['id_ecogarde'] !== -1) {
                if (!isset($binomeListe[$value['id_ecogarde']])) {
                    $info = $database->SQL("SELECT nom, prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$value['id_ecogarde']], true);
                    $ecogarde = $info[0]['nom']." ".$info[0]['prenom'];
                    $binomeListe[$value['id_ecogarde']] = $ecogarde;
                } else {
                    $ecogarde = $binomeListe[$value['id_ecogarde']];
                }
            } else {
                $ecogarde = "Erreur...";
            }

            // Initialisation des valeur vide
            $commune = "";
            $lieuxDit = "";

            // Apelle a la configuration
            $config = new Config('Commune.json');

            // recupert la commune et le lieux-dit
            if ($config->GetConfig($value['commune']) !== false) {
                $info = $config->GetConfig();
                foreach ($info as $forKey => $forValue) {
                    if ($forValue['Code'] === $value['commune']) {
                        $commune = $forKey;
                        $lieuxDit = $forValue['LieuDit'][$value['lieux_dit']];
                    }
                }
            }

            $configCode = new Config("Code.json");
            $retour = $configCode->GetConfig('Code_Mission');
            $configCode = $retour;

            // Verification mission administrative ou exterireur
            if ($value['mission_ext']) {
                $mission_type = "Mission de terain";
                $configCode = $configCode['MissionExt'];
                $option = "Ext_";
            } else {
                $mission_type = "Mission administratif";
                $configCode = $configCode['MissionInt'];
                $option = "Int_";
            }

            $name = $mission_type." : ".($configCode[$value['mission_code']]);

            // création du tableaux
            $MML = $MML."
            <tr>
                <td>".$value['id']."</td>
                <td>".$ecogarde."</td>
                <td>".$binome."</td>
                <td>".$value['date']."</td>
                <td>".$value['temps']."</td>
                <td>".$commune."</td>
                <td>".$lieuxDit."</td>
                <td>".$name."</td>
                <td><a href='".$this->router->generateUri("admin.get.mission", ['id' => $value['id']])."' class=\"badge badge-success\">Détails</a></td>
            </tr>";

        }

        return $this->renderer->render('@admin/planning', ["MML" => $MML]);
    }

    /**
     * apelle au detaile d'une mission
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_mission(ServerRequest $request): string
    {
        return (new GetMission())->call($this->renderer, $this->router, $request);
    }

    /**
     * apelle a la liste d'utilisateur
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_user(ServerRequest $request): string
    {
        // Apelle a la DB
        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes').";", [], true);

        $tables = "";

        foreach ($retour as $key => $value) {

            // Verification de mot de passe
            if ($value['password_modif'] === true) {
                $reset = "<span class=\"badge badge-warning\">En cours</span>";
            } else {
                $reset = "<a href='".$this->router->generateUri("admin.get.password", ['id' => $value['id']])."' class=\"badge badge-danger\">Réinitialisation</a>";
            }

            // creation de la tables
            $tables = $tables."
            <tr>
                <td>".$value['id']."</td>
                <td>".$value['nom']."</td>
                <td>".$value['prenom']."</td>
                <td>".$reset."</td>
            </tr>";
        }

        return $this->renderer->render('@admin/user', ['MML' => $tables]);
    }

    /**
     * met a jours un utilisateur en demande la modification de sont mot de passe
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_passwordModif(ServerRequest $request): string
    {
        $id = $request->getAttribute('id');

        $database = new Database();
        $database->SQL("UPDATE ".$database->Schemas('ecogardes')." SET password_modif = true WHERE id = ?;", [$id], false);

        return $this->renderer->render('@error/valide', ['slug' => 'Action valider', 'url' => $this->router->generateUri("admin.get.index", [])]);
    }

    /**
     * ajour d'un utilisateur
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Post_add(ServerRequest $request): string
    {
        // Verification des envoie d'information
        if (!isset($_POST['nom']) || !isset($_POST['prenom'])) {
            return $this->renderer->render('@error/error', ['slug' => 'Imposible de valider les infomation', 'url' => $this->router->generateUri("admin.get.index", [])]);
        }

        // Apelle a la DB
        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." WHERE nom = ? AND prenom = ?;", [strtolower($_POST['nom']), strtolower($_POST['prenom'])], true);

        // verification doublon
        if (!empty($retour)) {
            return $this->renderer->render('@error/error', ['slug' => 'Nom ou prénom déjà existant', 'url' => $this->router->generateUri("admin.get.index", [])]);
        }

        // Recuperation du dernier Id
        $retour = $database->SQL("SELECT id FROM ".$database->Schemas('ecogardes')." where id = (select max(id) from ".$database->Schemas('ecogardes').");", [], true);
        if (empty($retour)) {
            $id = 1;
        } else {
            $id = intval($retour[0]['id']) + 1;
        }

        // Hash un mot de passe temporaire
        $password = password_hash(strtolower($_POST['nom']).strtolower($_POST['prenom']), PASSWORD_DEFAULT);

        // Ajout dans la DB
        $database->SQL("INSERT INTO ".$database->Schemas('ecogardes')." (id, nom, prenom, password, admin, password_modif) VALUES (?,?,?,?,false,true);", [$id, strtolower($_POST['nom']), strtolower($_POST['prenom']), $password], false);

        return $this->renderer->render('@error/valide', ['slug' => 'Ecogarde ajouter', 'url' => $this->router->generateUri("admin.get.index", [])]);
    }

    /**
     * Affichage de l'extraction
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Get_extraction(ServerRequest $request): string
    {
        $option = (new Extract($this->renderer, $this->router))->ExtractPlanning($this->renderer);
        return $this->renderer->render('@admin/extraction', $option);
    }

    /**
     * créer une extraction de toute les mission
     *
     * @param ServerRequest $request
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function Get_extractionAll(ServerRequest $request): string
    {
        return (new Extract($this->renderer, $this->router))->Call();
    }

    /**
     * créer une extraction
     *
     * @param ServerRequest $request
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function Post_extraction(ServerRequest $request): string
    {

        $sql = [];
        $info = [];

        if ($_POST["select_commune"] !== "-1") {
            $commune = $_POST['select_commune'];
            $lieux_dit = $_POST['select_lieu_'.$commune];

            $config_commune = (new Config('Commune.json'))->GetConfig();

            $commune_name = "Erreur";
            $lieux_dit_name = "";

            foreach ($config_commune as $key => $value) {
                if ($value['Code'] == $commune) {
                    $commune_name = $key;

                    if ($lieux_dit !== '') {
                        $lieux_dit_name = $value['LieuDit'][(int)$lieux_dit];
                    }

                }
            }

            if ($lieux_dit === '') {
                array_push($sql, "commune = '".$commune."'");
                array_push($info, $commune_name);
            } else {
                array_push($sql, "commune = '".$commune."' AND lieux_dit = '".$lieux_dit."'");
                array_push($info, $commune_name." ".$lieux_dit_name);
            }

        }

        if ($_POST["select_binome"] !== "-1") {
            $id = $_POST['select_binome'];

            $database = new Database();
            $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$id], true);

            $ecogarde = $retour[0]['nom']." ".$retour[0]['prenom'];

            array_push($sql, "id_ecogarde = '".$id."'");

            array_push($info, $ecogarde);
        }

        if ($_POST["mois"] !== "-1") {
            if (intval($_POST["mois"]) < 10) {
                $mois = "0".$_POST["mois"];
            } else {
                $mois = $_POST["mois"];
            }

            array_push($sql, "date LIKE '%/".$mois."/%'");

            array_push($info, "Mois ".$mois);
        }

        $sql = join(" AND ", $sql);
        $info = join(" ", $info);

        return (new Extract($this->renderer, $this->router))->Call($sql, "extraction selective ".$info.".xls");
    }

    /**
     * créer une extraction pour les commune
     *
     * @param ServerRequest $request
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function Post_extractionCommune(ServerRequest $request): string
    {
        $commune = $_POST['select_commune'];
        $lieux_dit = $_POST['select_lieu_'.$commune];

        $config_commune = (new Config('Commune.json'))->GetConfig();

        foreach ($config_commune as $key => $value) {
            if ($value['Code'] == $commune) {
                $commune_name = $key;

                if ($lieux_dit !== '') {
                    $lieux_dit_name = $value['LieuDit'][(int)$lieux_dit];
                }

            }
        }

        if ($lieux_dit === '') {
            return (new Extract($this->renderer, $this->router))->Call("commune = '".$commune."'", "extraction commune ".$commune_name.".xls");
        } else {
            return (new Extract($this->renderer, $this->router))->Call("commune = '".$commune."' AND lieux_dit = '".$lieux_dit."'", "extraction commune ".$commune_name." = ".$lieux_dit_name.".xls");
        }
    }

    /**
     * Créer une extraction pour les date
     *
     * @param ServerRequest $request
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function Post_extractionDate(ServerRequest $request): string
    {
        $date_name = $_POST['date'];
        $date_name = str_replace('/', '_', $date_name);
        return (new Extract($this->renderer, $this->router))->Call("date = '".$_POST['date']."'", "extraction du ".$date_name.".xls");
    }

    /**
     * Créer une extraction pour les ecogarde
     *
     * @param ServerRequest $request
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function Post_extractionEcogarde(ServerRequest $request): string
    {
        if ($_POST['select_binome'] === '-1') {
            return $this->renderer->render('@error/error', ['slug' => 'Ecogarde introuvables', 'url' => $this->router->generateUri("admin.get.extraction", [])]);
        }

        $id = $_POST['select_binome'];

        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$id], true);

        $ecogarde = $retour[0]['nom']." ".$retour[0]['prenom'];

        return (new Extract($this->renderer, $this->router))->Call("id_ecogarde = '".$id."'", "extraction ecogarde ".$ecogarde.".xls");

    }
}