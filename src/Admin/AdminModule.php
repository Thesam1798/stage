<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 05/06/18
 * Time: 13:42
 */

namespace App\Admin;

use Framework\Renderer;
use Framework\Router;

class AdminModule
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * AdminModule constructor.
     * @param Router $router
     * @param Renderer $renderer
     */
    public function __construct(Router $router, Renderer $renderer)
    {
        // Ajoute le render et le router dans des variable pour les envoyer a des sub Controller
        $this->renderer = $renderer;
        $this->router = $router;


        // Apelle d'un sub controller
        $controller = new AdminController($this->renderer,$this->router);


        // Ajout du dossier de rendue pour ce module
        $this->renderer->addPath('admin', __DIR__ . DIRECTORY_SEPARATOR . 'views');


        // liste des url compatible
        // Methode(URL , [ ClassController , 'NameFunction'], 'NameRoute');
        $router->get($GLOBALS['BaseURL'].'admin', [$controller, 'Get_index'], 'admin.get.index');
        $router->get($GLOBALS['BaseURL'].'admin/extraction', [$controller, 'Get_extraction'], 'admin.get.extraction');
        $router->get($GLOBALS['BaseURL'].'admin/user', [$controller, 'Get_user'], 'admin.get.user');
        $router->get($GLOBALS['BaseURL'].'admin/user/password/{id:[0-9]+}', [$controller, 'Get_passwordModif'], 'admin.get.password');
        $router->get($GLOBALS['BaseURL'].'admin/user/add', [$controller, 'Get_add'], 'admin.get.add');
        $router->get($GLOBALS['BaseURL'].'admin/planning', [$controller, 'Get_planning'], 'admin.get.planning');
        $router->get($GLOBALS['BaseURL'].'admin/mission/{id:[0-9]+}', [$controller, 'Get_mission'], 'admin.get.mission');

        $router->get($GLOBALS['BaseURL'].'admin/extraction/all', [$controller, 'Get_extractionAll'], 'admin.get.extraction_all');

        $router->post($GLOBALS['BaseURL'].'admin/user/add', [$controller, 'Post_add'], 'admin.post.add');
        $router->post($GLOBALS['BaseURL'].'admin/extraction', [$controller, 'Post_extraction'], 'admin.post.extraction');
        $router->post($GLOBALS['BaseURL'].'admin/extraction/commune', [$controller, 'Post_extractionCommune'], 'admin.post.extractionCommune');
        $router->post($GLOBALS['BaseURL'].'admin/extraction/date', [$controller, 'Post_extractionDate'], 'admin.post.extractionDate');
        $router->post($GLOBALS['BaseURL'].'admin/extraction/ecogarde', [$controller, 'Post_extractionEcogarde'], 'admin.post.extractionEcogarde');

    }
}