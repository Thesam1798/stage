<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:32
 *
 * Html pour l'extraction
 */
?>

<?= $renderer->render('header') ?>

<main role="main">

    <div id="extract">
        <div class="album py-5 bg-light">
            <div class="container" style="text-align: center;">

                <h3>Quel type d'extraction ?</h3>
                <br>
                <a class="btn btn-warning" role="button"
                   href="<?= $router->generateUri("admin.get.extraction_all", []) ?>">Extraction génerale</a>
                <button class="btn btn-success" role="button" onclick="show_date()">Par date</button>
                <button class="btn btn-info" role="button" onclick="show_ecogarde()">Par ecogarde</button>
                <button class="btn btn-primary" role="button" onclick="show_def()">Par Définition</button>
                <a class="btn btn-outline-dark" role="button"
                   href="<?= $router->generateUri("admin.get.index", []) ?>">Retour</a>
            </div>
        </div>

    </div>

    <div id="defi">
        <div class="album py-5 bg-light">
            <div class="container" style="text-align: center;">
                <form id="form_add" method="post"
                      action="<?= $router->generateUri("admin.post.extraction", []) ?>">

                    <?= $HCL ?>

                    <?= $HLDL ?>

                    <br>

                    <div class="form-group">
                        <label for="mois">Extraction pour le mois</label>
                        <select class="form-control" id="mois" name="mois">
                            <option value="-1">Tous</option>
                            <option value="1">Janvier</option>
                            <option value="2">Février</option>
                            <option value="3">Mars</option>
                            <option value="4">Avril</option>
                            <option value="5">Mai</option>
                            <option value="6">Juin</option>
                            <option value="7">Juillet</option>
                            <option value="8">Août</option>
                            <option value="9">Septembre</option>
                            <option value="10">Octobre</option>
                            <option value="11">Novembre</option>
                            <option value="12">Décembre</option>
                        </select>
                    </div>

                    <br>

                    <?= $HB ?>

                    <br>


                    <button type="submit" class="btn btn-outline-success btn-block btn-lg">Valider</button>

                </form>
            </div>
        </div>
    </div>

    <div id="date">
        <div class="album py-5 bg-light">
            <div class="container" style="text-align: center;">
                <form id="form_add" method="post" action="<?= $router->generateUri("admin.post.extractionDate", []) ?>">

                    <div class="form-group">
                        <label for="date">Date : </label>
                        <input name="date" class="form-control" id="date" placeholder="Date" type="text" required/>
                    </div>

                    <br>

                    <button type="submit" class="btn btn-outline-success btn-block btn-lg">Valider</button>

                </form>
            </div>
        </div>
    </div>

    <div id="ecogarde">
        <div class="album py-5 bg-light">
            <div class="container" style="text-align: center;">
                <form id="form_add" method="post"
                      action="<?= $router->generateUri("admin.post.extractionEcogarde", []) ?>">

                    <?= $HB ?>

                    <br>

                    <button type="submit" class="btn btn-outline-success btn-block btn-lg">Valider</button>

                </form>
            </div>
        </div>
    </div>


</main>


<?= $renderer->render('footer') ?>

<script type="text/javascript">

    $(document).ready(function () {
        hide_all_select();
        hide_lieux_dits();
        $('#commune').hide();
        $('#date').hide();
        $('#ecogarde').hide();
        $('#defi').hide();
    });

    function hide_lieux_dits() {
        <?= $JLIEUDIT ?>
    }

    function call_select(id) {
        hide_all_select();
        $('#' + id).show();
    }

    function hide_all_select() {
        <?= $JMIH ?>
        <?= $JMEH ?>
    }

    function show_def() {
        $('#date').hide();
        $('#defi').show();
        $('#ecogarde').hide();
    }

    function show_date() {
        $('#date').show();
        $('#ecogarde').hide();
        $('#defi').hide();

    }

    function show_ecogarde() {
        $('#date').hide();
        $('#ecogarde').show();
        $('#defi').hide();

    }

    <?= $JMEXT ?>
</script>

<script type="application/javascript">

    var locaux = false;
    var binome = false;

    $('#menu').show();
    $('#add_planning').hide();
    $('#my_planning').hide();
    $('#div_locaux').hide();
    $('#div_exterieur').show();
    $('#div_binome').hide();


    function show_menu() {
        $('#menu').show();
        $('#add_planning').hide();
        $('#my_planning').hide();
    }

    function show_add() {
        $('#menu').hide();
        $('#add_planning').show();
        $('#my_planning').hide();
    }

    function show_my() {
        $('#menu').hide();
        $('#add_planning').hide();
        $('#my_planning').show();
    }

    function show_locaux() {
        if (locaux === true) {
            locaux = false;
            $('#div_locaux').hide();
            $('#div_exterieur').show();
        } else {
            locaux = true;
            $('#div_locaux').show();
            $('#div_exterieur').hide();
        }
    }

    function show_binome() {
        if (binome === true) {
            binome = false;
            $('#div_binome').hide();
        } else {
            binome = true;
            $('#div_binome').show();
        }
    }


    function commune(){
        var selectBox = document.getElementById("select_commune");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        hide_lieux_dits();
        call_select('lieu_'+selectedValue);
    }


    $(document).ready(function () {
        var date_input = $('input[name="dateAU"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
            weekStart: 1,
            orientation: "top",
            language: 'fr'
        };
        date_input.datepicker(options);
    });

    $(document).ready(function () {
        var date_input = $('input[name="dateDU"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
            weekStart: 1,
            orientation: "top",
            language: 'fr'
        };
        date_input.datepicker(options);
    });

    $('#time_debut').timepicker({
        template: 'dropdown',
        appendWidgetTo: 'body',
        maxHours : 24,
        showSeconds : false,
        showMeridian: false,
        secondStep : 5,
        defaultTime : 'current',
        icons : {
            up: '"></span><i class="fas fa-angle-up"></i><span class="',
            down: '"></span><i class="fas fa-angle-down"></i><span class="'
        }
    });

    $('#time_fin').timepicker({
        template: 'dropdown',
        appendWidgetTo: 'body',
        maxHours : 24,
        showSeconds : false,
        showMeridian: false,
        secondStep : 5,
        defaultTime : 'current',
        icons : {
            up: '"></span><i class="fas fa-angle-up"></i><span class="',
            down: '"></span><i class="fas fa-angle-down"></i><span class="'
        }
    });

    $("[name='locaux']").bootstrapSwitch();
    $("[name='binome']").bootstrapSwitch();

</script>