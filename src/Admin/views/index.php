<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:32
 *
 * Html pour l'index administration
 */
?>

<?= $renderer->render('header') ?>

    <main role="main">

        <div id="menu">
            <div class="album py-5 bg-light">
                <div class="container" style="text-align: center;">

                    <h3>Que veux-tu faire ?</h3>
                    <br>
                    <a class="btn btn-primary" role="button"
                       href="<?= $router->generateUri("admin.get.extraction", []) ?>">Extraction</a>

                    <a class="btn btn-success" role="button"
                       href="<?= $router->generateUri("admin.get.planning", []) ?>">Voir les plannings</a>

                    <a class="btn btn-info" role="button" href="<?= $router->generateUri("admin.get.user", []) ?>">Gestion
                                                                                                                   des
                                                                                                                   utilisateurs</a>

                    <a class="btn btn-outline-dark" href="<?= $router->generateUri("user.get.index",[]) ?>" role="button">Retour</a>
                </div>
            </div>

        </div>

    </main>

<?= $renderer->render('footer') ?>