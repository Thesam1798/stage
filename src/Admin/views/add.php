<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:32
 *
 * HTML pour l'ajout d'utilisateur
 */
?>

<?= $renderer->render('header') ?>

    <main role="main">
        <div id="add_planning" class="album py-5 bg-light">
            <div class="container" style="text-align: center;">

                <h3>Ajouter d'un utilisateur</h3>
                <br>
                <a class="btn btn-outline-dark" role="button" href="<?= $router->generateUri("admin.get.index", []) ?>"><i
                            class="fas fa-arrow-left"></i>
                    Retour
                </a>
                <br>
                <br>

                <form id="form_add" method="post" action="<?= $router->generateUri("admin.post.add", []) ?>">

                    <div class="form-group">
                        <label for="nom">Nom : </label>
                        <input name="nom" class="form-control" id="nom" placeholder="Jone" type="text" required/>
                    </div>

                    <div class="form-group">
                        <label for="prenom">Prenom : </label>
                        <input name="prenom" class="form-control" id="prenom" placeholder="Doe" type="text" required/>
                    </div>


                    <br>

                    <button type="submit" class="btn btn-outline-success btn-block btn-lg">Valider</button>
                </form>
            </div>
        </div>
    </main>

<?= $renderer->render('footer') ?>