<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/06/18
 * Time: 13:30
 *
 * detaille d'une misison
 */
?>


<?= $renderer->render('header') ?>

<main role="main">

    <div id="menu">
        <div class="album py-5 bg-light">
            <div class="container" style="text-align: center;">
                <a class="btn btn-dark" role="button" href="<?= $router->generateUri("admin.get.index", []) ?>"><i
                            class="fas fa-arrow-left"></i> Retour</a>
            </div>
        </div>
    </div>

    <div id="ValideMission" class="album py-5 bg-light">
        <div class="container" style="text-align: center;">

            <h3>Mission</h3>
            <br>
            <?= $Affichage ?>
        </div>
    </div>

</main>

<?= $renderer->render('footer'); ?>
