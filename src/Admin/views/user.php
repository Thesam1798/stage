<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:32
 *
 * liste de tous les ecogarde
 */
?>

<?= $renderer->render('header') ?>

<main role="main">

    <div id="my_planning" class="album py-5 bg-light">
        <div class="container" style="text-align: center;">

            <h3>Ecogarde</h3>
            <br>
            <a class="btn btn-outline-dark" role="button" href="<?= $router->generateUri("admin.get.index", []) ?>"><i
                        class="fas fa-arrow-left"></i>
                Retour
            </a>
            <br>
            <br>
            <a class="btn btn-info" role="button" href="<?= $router->generateUri("admin.get.add", []) ?>">
                Ajouter un utilisateur
            </a>
            <br>
            <br>

            <table id="tables_MML" class="display table" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Réinitialisation</th>
                </tr>
                </thead>
                <tbody>
                <?= $MML ?>
                </tbody>
            </table>
        </div>
    </div>


</main>

<?= $renderer->render('footer') ?>
