<?php
/**
 * Created by IntelliJ IDEA.
 * User: AlexandreDebris
 * Date: 11/06/2018
 * Time: 13:45
 *
 * Fichier d'extraction pour la partie administrateur
 *
 */

namespace App\Admin\SubController;

use Framework\Config;
use Framework\Database;
use Framework\Renderer;
use Framework\Router;
use Framework\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class Extract
{

    /**
     * @var Renderer
     */
    private $renderer;
    /**
     * @var Router
     */
    private $router;

    /**
     * Extract constructor.
     * Rends les variables utilisables par la class
     *
     * @param Renderer $renderer
     * @param Router $router
     */
    public function __construct(Renderer $renderer, Router $router)
    {
        $this->renderer = $renderer;
        $this->router = $router;
    }

    /**
     * Fonction permettant de créer touts les champs d'un formulaire pour l'extraction par date ecogarde ou par lieux.
     *
     * @param Renderer $renderer
     * @return array
     */
    public function ExtractPlanning(Renderer $renderer): array
    {
        $mission = new Config("Mission.json");
        $missionExt = $mission->GetConfig("MissionExt");
        $missionInt = $mission->GetConfig("MissionInt");

        $codeConfig = (new Config("Code.json"))->GetConfig("Mission_Code");
        $codeInt = $codeConfig["MissionInt"];
        $codeExt = $codeConfig["MissionExt"];

        $tables = new Config("Tables.json");

        $html_mission_int_liste = "";
        $html_mission_int_select = "";
        $js_mission_int_hide = "";
        $html_mission_int_champ = "";
        $js_mission_extension = "";

        foreach ($missionInt as $key => $value) {

            $code = $codeInt[$key];
            $name = $key;
            $html_mission_int_liste = $html_mission_int_liste."<div class=\"custom-control custom-radio\"><input type=\"radio\" id=\"".$code."\" name=\"check\" value='".$code."' class=\"custom-control-input\"><label class=\"custom-control-label\" for=\"".$code."\" onclick=\"call_select('div_".$code."');\">".$name."</label></div>";
            if (!empty($value)) {
                $option = "";
                foreach ($value as $id => $nom) {
                    $option = $option."<option value=\"".$id."\">".$nom."</option>";
                }
                $html_mission_int_select = $html_mission_int_select."<div id=\"div_".$code."\"><div class=\"form-group\"><label for=\"select_".$code."\">".$name."</label><select name=\"select_".$code."\" class=\"custom-select\" id=\"select_".$code."\"><option value=\"0\">Choisir...</option>".$option."</select></div></div>";
            }
            $js_mission_int_hide = $js_mission_int_hide."$('#div_".$code."').hide();";
        }

        $html_mission_ext_liste = "";
        $html_mission_ext_select = "";
        $html_mission_ext_champ = "";
        $js_mission_ext_hide = "";

        $database = new Database();
        $retour = $database->SQL("SELECT id FROM ".$database->Schemas('mission')." where id = (select max(id) from ecogarde.mission);", [], true);

        if (empty($retour)) {
            $missionId = 1;
        } else {
            $missionId = intval($retour[0]['id']) + 1;
        }

        $session = new Session();
        $retour = json_decode($session->GetSession('User'), true);

        $ecogardeId = $retour['id'];

        foreach ($missionExt as $key => $value) {
            $code = $codeExt[$key];
            $name = $key;
            $extension = "";
            $tablesListe = $tables->GetConfig($code);

            if ($tablesListe === $code) {
                $tablesInfo = $tables->GetConfig("Ext_".$name)['Information'];

                $js_mission_extension = $js_mission_extension."function show_all_extension_".$code."(){";

                $extension = "show_all_extension_".$code."();";

                foreach ($tablesInfo as $tablesInfoKey => $tablesInfoValue) {

                    if ($tablesInfoKey === 'Option') {

                    } else {

                        if (isset($tablesInfoValue['Afficher'])) {
                            $champName = $tablesInfoValue['Afficher'];
                        } else {
                            $champName = $tablesInfoValue['Nom'];
                        }

                        $champType = $tablesInfoValue['Type'];
                        $champCode = $code."_".crc32($champName);

                        $js_mission_ext_hide = $js_mission_ext_hide."$('#champ_".$champCode."').hide();";

                        $js_mission_extension = $js_mission_extension."$('#champ_".$champCode."').show();";

                        if (is_array($tablesInfoValue['Type'])) {

                            if ($tablesInfoValue['Type']['Option'] === 'REF') {

                                $ref = $tablesInfoValue['Type']['REF'];

                                if (isset($ref['Tables'])) {
                                    if ($ref['Tables'] === "Mission") {
                                        $html_mission_ext_champ = $html_mission_ext_champ."<input type=\"hidden\" name=\"champ_".$champCode."_id_mission\" id=\"champ_".$champCode."_id_mission\" value=\"".$missionId."\">";
                                    } elseif ($ref['Tables'] === "Ecogarde") {
                                        $html_mission_ext_champ = $html_mission_ext_champ."<input type=\"hidden\" name=\"champ_".$champCode."_id_ecogarde\" id=\"champ_".$champCode."_id_ecogarde\" value=\"".$ecogardeId."\">";
                                    }
                                }
                            }
                        }

                        if ($champType === 'int') {
                            $html_mission_ext_champ = $html_mission_ext_champ."<div class=\"form-group\" id='champ_".$champCode."'><label for=\"champ_".$champCode."_input\">".$champName."</label><input type=\"number\" class=\"form-control\" id=\"champ_".$champCode."_input\" name=\"champ_".$champCode."_input\"></div>";
                        } elseif ($champType === 'text') {
                            $html_mission_ext_champ = $html_mission_ext_champ."<div class=\"form-group\" id='champ_".$champCode."'><label for=\"champ_".$champCode."_input\">".$champName."</label><textarea class=\"form-control\" id=\"champ_".$champCode."_input\" rows=\"3\" maxlength=\"500\" name=\"champ_".$champCode."_input\"></textarea></div>";
                        } elseif ($champType === 'LIST') {

                            $option = "";

                            foreach ($tablesInfoValue['LIST'] as $listeKey => $listeValue) {
                                $option = $option."<option value=\"".$listeValue."\">".$listeValue."</option>";
                            }

                            $html_mission_ext_champ = $html_mission_ext_champ."<div id='champ_".$champCode."'><div class=\"form-group\"><label for=\"champ_".$champCode."_input\">".$champName."</label><select id=\"champ_".$champCode."_input\" class=\"custom-select\" name=\"champ_".$champCode."_input\"><option value=\"0\" selected>Choisir...</option>".$option."</select></div></div>";
                        }

                    }
                }

                $js_mission_extension = $js_mission_extension."}";

            }

            $html_mission_ext_liste = $html_mission_ext_liste."<div class=\"custom-control custom-radio\"><input type=\"radio\" id=\"".$code."\" name=\"check\" value='".$code."' class=\"custom-control-input\"><label class=\"custom-control-label\" for=\"".$code."\" onclick=\"call_select('div_".$code."');".$extension."\">".$name."</label></div>";
            if (!empty($value)) {
                $option = "";
                foreach ($value as $id => $nom) {
                    $option = $option."<option value=\"".$id."\">".$nom."</option>";
                }
                $html_mission_ext_select = $html_mission_ext_select."<div id=\"div_".$code."\"><div class=\"form-group\"><label for=\"select_".$code."\">Option</label><select name=\"select_".$code."\" class=\"custom-select\" id=\"select_".$code."\"><option value=\"0\" selected>Choisir...</option>".$option."</select></div></div>";
            }
            $js_mission_ext_hide = $js_mission_ext_hide."$('#div_".$code."').hide();";
        }

        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes').";", [], true);
        $option = "";

        foreach ($retour as $key => $value) {
            $option = $option."<option value=\"".$value['id']."\">".$value['nom']." ".$value['prenom']."</option>";
        }

        $html_binome = "<div class=\"form-group\"><label for=\"select_binome\">Selection d'un binome</label><select class=\"form-control\" id=\"select_binome\" name='select_binome'><option value=\"-1\" selected>Choisir...</option>".$option."</select></div>";

        $html_lieu_dit_liste = "";
        $optionCommune = "";
        $js_lieu_dit = "";

        $config = new Config('Commune.json');
        $retour = $config->GetConfig();

        foreach ($retour as $key => $value) {
            $optionCommune = $optionCommune."<option value=\"".$value['Code']."\">".$key."</option>";

            $optionLieu = "";

            foreach ($value['LieuDit'] as $lieuKey => $lieuValue) {
                $optionLieu = $optionLieu."<option value=\"".$lieuKey."\">".$lieuValue."</option>";
            }

            $html_lieu_dit_liste = $html_lieu_dit_liste."<div class=\"form-group\" id='lieu_".$value['Code']."'><label for=\"select_lieu_".$value['Code']."\">Lieu Dit pour la commune : ".$key."</label><select class=\"form-control\" id=\"select_lieu_".$value['Code']."\" name='select_lieu_".$value['Code']."'><option value=\"\" selected>Choisir...</option>".$optionLieu."</select></div>";

            $js_lieu_dit = $js_lieu_dit."$('#lieu_".$value['Code']."').hide();";

        }

        $html_commune_liste = "<div class=\"form-group\"><label for=\"select_commune\">Commune</label><select class=\"form-control\" id=\"select_commune\" name='select_commune' onchange=\"commune();\" required><option value=\"-1\" selected>Choisir...</option>".$optionCommune."</select></div>";

        return [
            'MIL' => $html_mission_int_liste,
            'MIS' => $html_mission_int_select,
            'MIC' => $html_mission_int_champ,
            'JMIH' => $js_mission_int_hide,
            'MEL' => $html_mission_ext_liste,
            'MES' => $html_mission_ext_select,
            'MEC' => $html_mission_ext_champ,
            'JMEH' => $js_mission_ext_hide,
            'JMEXT' => $js_mission_extension,
            'HB' => $html_binome,
            'HCL' => $html_commune_liste,
            'HLDL' => $html_lieu_dit_liste,
            'JLIEUDIT' => $js_lieu_dit
        ];
    }

    /**
     * Fonction permettant de créer le fichier XLS
     *
     * Option SQL pour un WHERE
     * @param String $optionSQL
     * @param string $file_name
     *
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function Call(String $optionSQLRequest = "", string $file_name = "extraction.xls"): string
    {
        // Apelle a la configuration
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Apelle a la DB
        $database = new Database();

        // Verification de l'option
        if ($optionSQLRequest === "") {
            $retourDB = $database->SQL("SELECT * FROM ".$database->Schemas('mission')." ORDER BY mission_code ASC;", [], true);
        } else {
            $retourDB = $database->SQL("SELECT * FROM ".$database->Schemas('mission')." WHERE ".$optionSQLRequest." ORDER BY mission_code ASC;", [], true);
        }

        // Verification de tables vides
        if (empty($retourDB)) {
            return $this->renderer->render('@error/error', ['slug' => "La table demandée est vide", 'url' => $this->router->generateUri('admin.get.extraction', [])]);
        }

        // liste des utilisateur connue lors du for
        $user_liste = [];

        // liste pour le tableaux
        $alphabet = range('A', 'Z');
        $alphabetUse = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        // ligne en cours
        $ligne = 1;

        // ajour de cellule dans le tableaux
        $sheet->setCellValue($alphabet[0].$ligne, 'Id');
        $sheet->setCellValue($alphabet[1].$ligne, 'Ecogarde');
        $sheet->setCellValue($alphabet[2].$ligne, 'Binome');
        $sheet->setCellValue($alphabet[3].$ligne, 'Date');
        $sheet->setCellValue($alphabet[4].$ligne, 'Temps');
        $sheet->setCellValue($alphabet[5].$ligne, 'Commune');
        $sheet->setCellValue($alphabet[6].$ligne, 'Lieux Dits');
        $sheet->setCellValue($alphabet[7].$ligne, 'Type de mission');
        $sheet->setCellValue($alphabet[8].$ligne, 'Mission');
        $sheet->setCellValue($alphabet[9].$ligne, 'Option');
        $sheet->setCellValue($alphabet[11].$ligne, 'Remarque');

        // saut de ligne
        $ligne = $ligne + 2;

        $optionAll = [];

        // boucle pour la creation de ligne pour chaque mission
        foreach ($retourDB as $key => $value) {

            // Apelle a la configuration
            $configCode = new Config("Code.json");
            $configTables = new Config("Tables.json");
            $communeConfig = new Config("Commune.json");
            $missionConfig = new Config("Mission.json");

            $mission = $value;

            $retour = $configCode->GetConfig('Code_Mission');
            $configCode = $retour;

            // verification mission administrative ou de terain
            if ($mission['mission_ext']) {
                $mission_type = "Mission de terain";
                $configCode = $configCode['MissionExt'];
                $option = "Ext_";
            } else {
                $mission_type = "Mission administratif";
                $configCode = $configCode['MissionInt'];
                $option = "Int_";
            }

            // nom de la misison
            $name_brute = $configCode[$mission['mission_code']];

            // nom code misison
            $name = $option.($configCode[$mission['mission_code']]);

            // Apelle a la configuration
            $retour = $configTables->GetConfig($name);
            $configTables = $retour;

            $option = [];

            // verification d'extension de mission
            if ($retour !== false) {
                $option = [];

                // Apelle DB
                $optionSQL = $database->SQL("SELECT * FROM ".$database->Schemas(strtolower($name))." WHERE id_mission = ?", [$mission['id']], true);

                // Si vide erreur
                if (empty($optionSQL)) {
                    return $this->renderer->render('@error/error', ['slug' => "Imposible de trouver les detaille de la mission"]);
                }

                $count = 0;

                // ajout dans une liste pour chaque element de la mission
                foreach ($configTables['Information'] as $keyFor => $valueFor) {
                    if (isset($valueFor['Afficher'])) {
                        $nom = $valueFor['Afficher'];
                    } else {
                        $nom = $valueFor['Nom'];
                    }

                    if ($nom !== "id" && $nom !== "id_mission" && $nom !== "id_ecogarde") {
                        array_push($option, [$nom => $optionSQL[0][$count]]);
                    }

                    $count = $count + 1;
                }
            }

            // liste les ecogarde et les ajoute dans $user_liste
            if ($mission['id_ecogarde'] != "-1") {
                if (isset($user_liste[$mission['id_ecogarde']])) {
                    $ecogarde = $user_liste[$mission['id_ecogarde']];
                } else {
                    $ecogarde = $database->SQL("SELECT nom,prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$mission['id_ecogarde']], true);
                    $ecogarde = $ecogarde[0]['nom']." ".$ecogarde[0]['prenom'];
                    $user_liste[$mission['id_ecogarde']] = $ecogarde;
                }
            } else {
                $ecogarde = "Erreur";
            }

            // liste les binome et les ajoute dans $user_liste
            if ($mission['binome'] != "-1") {
                if (isset($user_liste[$mission['binome']])) {
                    $binome = $user_liste[$mission['binome']];
                } else {
                    $binome = $database->SQL("SELECT nom,prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$mission['binome']], true);
                    $binome = $binome[0]['nom']." ".$binome[0]['prenom'];
                    $user_liste[$mission['binome']] = $binome;
                }
            } else {
                $binome = "Aucun binome";
            }

            // Initialisation des valeur vide
            $commune = "";

            // Apelle a la configuration
            $communeConfig = $communeConfig->GetConfig();

            // recupert la commune et le lieux-dit
            foreach ($communeConfig as $keyFor => $valueFor) {
                if ($valueFor['Code'] === $mission['commune']) {
                    $commune = $keyFor;
                    $lieuxdit = $valueFor['LieuDit'][$mission['lieux_dit']];
                }
            }

            // verification d'option suplemnetaire
            if (empty($option)) {
                $mission_option = $missionConfig->GetConfig($name_brute);
                if (!empty($mission_option)) {
                    $mission_option = $mission_option[$mission['mission_option']];
                } else {
                    $mission_option = "Aucune option";
                }
            } else {
                $mission_option = "Détailles";
            }

            // ajout de ligne
            $sheet->setCellValue($alphabet[0].$ligne, $mission['id']);
            $sheet->setCellValue($alphabet[1].$ligne, $ecogarde);
            $sheet->setCellValue($alphabet[2].$ligne, $binome);
            $sheet->setCellValue($alphabet[3].$ligne, $mission['date']);
            $sheet->setCellValue($alphabet[4].$ligne, $mission['temps']);
            $sheet->setCellValue($alphabet[5].$ligne, $commune);
            $sheet->setCellValue($alphabet[6].$ligne, $lieuxdit);
            $sheet->setCellValue($alphabet[7].$ligne, $mission_type);

            $sheet->setCellValue($alphabet[8].$ligne, $name_brute);
            $sheet->setCellValue($alphabet[9].$ligne, $mission_option);

            $sheet->setCellValue($alphabet[11].$ligne, $mission['remarque']);

            if (!empty($option)) {
                foreach ($option as $valueFor) {
                    foreach ($valueFor as $keyFor => $itemFor) {
                        $colone_name = $keyFor;
                        $value = $itemFor;

                        if (isset($optionAll[$colone_name])) {
                            $optionAll[$colone_name][$ligne] = $value;
                        } else {
                            $optionAll[$colone_name] = [];
                            $optionAll[$colone_name][$ligne] = $value;
                        }
                    }
                }
            }
            // saut de ligne
            $ligne = $ligne + 1;
        }

        foreach ($optionAll as $key => $ligne) {
            $col = (count($alphabetUse));
            if ($col >= 26) {
                $lettre = $alphabet[$col / 26 - 1].$alphabet[$col - 26];
            } else {
                $lettre = $alphabet[$col];
            }

            foreach ($ligne as $ligneKey => $value) {
                $sheet->setCellValue($lettre."1", $key);
                $sheet->setCellValue($lettre.$ligneKey, $value);
            }

            array_push($alphabetUse, $col);
        }

        // chemain du fichier
        $file = (dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$file_name);

        // Verification si le fichier existe si oui supression
        if (file_exists($file)) {
            unlink($file);
        }

        // Création du fichier
        $writer = new Xls($spreadsheet);
        $writer->save($file_name);

        //Redirection pour le telecharger
        $URL = $GLOBALS['BaseURL']."".$file_name;
        header('Location: '.$URL);

        //Fin
        die();
    }
}