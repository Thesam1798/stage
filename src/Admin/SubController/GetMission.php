<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/06/18
 * Time: 16:01
 */

namespace App\Admin\SubController;

use Framework\Config;
use Framework\Database;
use Framework\Renderer;
use Framework\Router;
use GuzzleHttp\Psr7\ServerRequest;

class GetMission
{

    /**
     * generation d'un tableux pour les missions
     *
     * @param Renderer $renderer
     * @param Router $router
     * @param ServerRequest $request
     * @return string
     */
    public function Generator(Renderer $renderer, Router $router, ServerRequest $request): string
    {
        // recuperation de l'atribut de la mission
        $id = $request->getAttribute('id');
        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('mission')." WHERE id = ?;", [$id], true);

        $mission = $retour[0];

        // Apelle a la configuration
        $configCode = new Config("Code.json");
        $retour = $configCode->GetConfig('Code_Mission');
        $configCode = $retour;

        // Verification mission administrative ou exterireur
        if ($mission['mission_ext']) {
            $mission_type = "Mission de terain";
            $configCode = $configCode['MissionExt'];
            $option = "Ext_";
        } else {
            $mission_type = "Mission administratif";
            $configCode = $configCode['MissionInt'];
            $option = "Int_";
        }

        // Code nom misison
        $name = $option.($configCode[$mission['mission_code']]);

        // Apelle a la configuration
        $configTables = new Config("Tables.json");
        $retour = $configTables->GetConfig($name);
        $configTables = $retour;

        // Verification des extension de mission
        if ($retour === false) {
            // Pas d'option
            $option = "";
        } else {
            // Apelle a la DB
            $optionSQL = $database->SQL("SELECT * FROM ".$database->Schemas(strtolower($name))." WHERE id_mission = ?", [$mission['id']], true);

            // Erreur si vide
            if (empty($optionSQL)) {
                header('Location: '.$router->generateUri("error.get.error", []));
                die();
            }

            // ajout des ligne HTML
            $option = "<div class='col-6' style='text-align: right;'><p class='col-8' style='text-align: center'>Informations supplementaires sur la mission</p>";
            $count = 0;

            // ajout pour chaque information
            foreach ($configTables['Information'] as $key => $value) {
                if (isset($value['Afficher'])) {
                    $nom = $value['Afficher'];
                } else {
                    $nom = $value['Nom'];
                }

                if ($nom !== "id" && $nom !== "id_mission" && $nom !== "id_ecogarde") {
                    $option = $option."
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>".$nom." : </div>                    
                        <div class='col-4' style='text-align: right'>".$optionSQL[0][$count]."</div>
                    </div>";
                }

                $count = $count + 1;
            }

            $option = $option."</div>";

        }

        // Identifie le binome
        if ($mission['binome'] != "-1") {

            $binome = $database->SQL("SELECT nom,prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$mission['binome']], true);

            $binome = $binome[0]['nom']." ".$binome[0]['prenom'];

        } else {
            $binome = "Aucun binome";
        }

        // Initialisation des valeur vide
        $commune = "";

        // Apelle a la configuration
        $communeConfig = new Config("Commune.json");
        $communeConfig = $communeConfig->GetConfig();

        // recupert la commune et le lieux-dit
        foreach ($communeConfig as $key => $value) {
            if ($value['Code'] === $mission['commune']) {
                $commune = $key;
                $lieuxdit = $value['LieuDit'][$mission['lieux_dit']];
            }
        }

        // Créer l'affichage HTML
        $affichage = "
            <div class='row'>
                <div class='col-6' style='text-align: left;'>
                
                    <p class='col-8' style='text-align: center'>Informations sur la mission</p>
                
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Id de la mission : </div>
                        <div class='col-4' style='text-align: right'>".$mission['id']."</div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Binome : </div>
                        <div class='col-4' style='text-align: right'>".$binome."</div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Date : </div>
                        <div class='col-4' style='text-align: right'>".$mission['date']."</div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Temps Passé : </div>
                        <div class='col-4' style='text-align: right'>".$mission['temps']."</div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Commune : </div>
                        <div class='col-4' style='text-align: right'>".$commune."</div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Lieux-dits : </div>
                        <div class='col-4' style='text-align: right'>".$lieuxdit."</div>                    
                    </div>
                    
                    <div class='row'>
                        <div class='col-4' style='text-align: left'>Type de mission : </div>
                        <div class='col-4' style='text-align: right'>".$mission_type."</div>                    
                    </div>
                    
                    <br>
                    
                    <div class='row'>
                        <div class='col-auto' style='text-align: center'>Remarque : </div>
                    </div>

                    <div class='row'>
                        <div class='col-12' style='text-align: justify-all'>".$mission['remarque']."</div>
                    </div>
                
                </div>
                
                ".$option."
                
            </div>";

        // retour de l'affichage
        return $affichage;
    }

    /**
     * apelle pour la generation
     *
     * @param Renderer $renderer
     * @param Router $router
     * @param ServerRequest $request
     * @return string
     */
    public function call(Renderer $renderer, Router $router, ServerRequest $request): string
    {
        $affichage = $this->Generator($renderer, $router, $request);
        return $renderer->render('@admin/ValideMission', ['Affichage' => $affichage]);
    }

}