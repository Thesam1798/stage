<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 21/01/2018
 * Time: 11:42
 */

namespace App\Error;

use Framework\Renderer;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class ErrorModule
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * ErrorModule constructor.
     * @param Router $router
     * @param Renderer $renderer
     */
    public function __construct(Router $router, Renderer $renderer)
    {
        // Ajoute le render et le router dans des variable pour les envoyer a des sub Controller
        $this->renderer = $renderer;
        $this->router = $router;


        // Ajout du dossier de rendue pour ce module
        $this->renderer->addPath('error', __DIR__ . DIRECTORY_SEPARATOR . 'views');


        // liste des url compatible
        // Methode(URL , [ ClassController , 'NameFunction'], 'NameRoute');
        $router->get($GLOBALS['BaseURL'].'valide', [$this, 'Get_valide'], 'error.get.valide');
        $router->get($GLOBALS['BaseURL'].'error', [$this, 'Get_error'], 'error.get.error');
    }

    /**
     * Apelle a l'affichage Valide
     * @return string
     */
    public function Get_valide(): string
    {
        return $this->renderer->render('@error/valide', ['slug' => 'Validation générale.','url' => $GLOBALS['BaseURL']]);
    }

    /**
     * Apelle a l'affichage Error
     * @return string
     */
    public function Get_error(): string
    {
        return $this->renderer->render('@error/error', ['slug' => 'Erreur générale.','url' => $GLOBALS['BaseURL']]);
    }
}