<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 15/05/18
 * Time: 12:35
 *
 * Generation d'une page de validation
 */

if (!isset($slug)) {
    $slug = "Erreur introuvable.";
} else {
    if ($slug === "") {
        $slug = "Erreur introuvable.";
    }
}

if (!isset($url)) {
    $url = $GLOBALS['BaseURL'];
} else {
    if ($url === "") {
        $url = $GLOBALS['BaseURL'];
    }
}
?>

<?= $renderer->render('header') ?>

<link href="/assets/css/floating-labels.css" rel="stylesheet">

<body class="bg-dark">
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Information !</div>
            <div class="card-body">
                <div class="alert alert-info" role="alert">
                    <h4><?= $slug ?></h4><br>
                    <h5>Redirection dans 5 secondes.</h5>
                </div>
            </div>
        </div>
    </div>
</body>

<script type="application/javascript">

    //function redirect() {
    //    setTimeout(function () {
    //        window.location = "<?= $url ?>";
    //    }, 3000);
    //}

    redirect();
</script>

<?= $renderer->render('footer') ?>
