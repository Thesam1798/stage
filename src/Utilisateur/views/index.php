<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:32
 */
?>

<?= $renderer->render('header') ?>

<main role="main">

    <div id="menu">
        <div class="album py-5 bg-light">
            <div class="container" style="text-align: center;">

                <h3>Que veux-tu faire ?</h3>
                <br>
                <button class="btn btn-primary" role="button" onclick="show_add()">Ajouter un planning</button>
                <button class="btn btn-success" role="button" onclick="show_my()">Voir les plannings</button>
                <?php
                if (isset($_SESSION["Admin"])){
                    ?>
                    <a class="btn btn-outline-dark" href="<?= $router->generateUri("admin.get.index",[]) ?>" role="button">Administration</a>
                    <?php
                }
                ?>
            </div>
        </div>

        <?= $BI ?>

    </div>

    <div id="add_planning" class="album py-5 bg-light">
        <div class="container" style="text-align: center;">

            <h3>Ajouter d'un planning</h3>
            <br>
            <button class="btn btn-outline-dark" role="button" onclick="show_menu()"><i class="fas fa-arrow-left"></i>
                Retour
            </button>
            <br>
            <br>

            <br>
            <br>

            <form id="form_add" method="post" action="<?= $router->generateUri("user.post.add",[]) ?>">

                <div class="form-group">
                    <label for="binome">Mission en binome ?</label><br>
                    <input type="checkbox" name="binome" id="binome" onchange="show_binome()">
                </div>

                <div class="form-group">
                    <label for="date">Date : </label>
                    <input name="date" class="form-control" id="date" placeholder="Date" type="text" required/>
                </div>
                <div class="form-group">
                    <label for="time_debut">Heure du début</label>
                    <input name="time_debut" id="time_debut" type="text" class="form-control input-small"
                           placeholder="9H00" required>
                </div>
                <div class="form-group">
                    <label for="time_fin">Heure du fin</label>
                    <input name="time_fin" id="time_fin" type="text" class="form-control input-small"
                           placeholder="12H05" required>
                </div>

                <?= $HCL ?>

                <?= $HLDL ?>

                <div id="div_binome">

                    <?= $HB ?>

                </div>


                <div class="form-group">
                    <label for="remarque">Remarque (Optionnel)</label>
                    <textarea name="remarque" class="form-control" id="remarque" rows="3" maxlength="500"></textarea>
                </div>

                <div class="form-group">
                    <label for="locaux">Mission administrative ?</label><br>
                    <input type="checkbox" name="locaux" id="locaux" onchange="show_locaux()">
                </div>

                <div id="div_locaux">

                    <p>Mission administrative</p>

                    <?= $MIL ?>

                    <br>

                    <?= $MIS ?>

                    <br>

                    <?= $MIC ?>

                </div>

                <div id="div_exterieur">

                    <p>Mission en exterieur</p>

                    <?= $MEL ?>

                    <br>

                    <?= $MES ?>

                    <br>

                    <?= $MEC ?>

                </div>

                <br>

                <button type="submit" class="btn btn-outline-success btn-block btn-lg">Valider</button>
            </form>
        </div>
    </div>

    <div id="my_planning" class="album py-5 bg-light">
        <div class="container" style="text-align: center;">

            <h3>Mes plannings</h3>
            <br>
            <button class="btn btn-outline-dark" role="button" onclick="show_menu()"><i class="fas fa-arrow-left"></i>
                Retour
            </button>
            <br>
            <br>

            <table id="tables_MML" class="display table" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Binome</th>
                    <th>Date</th>
                    <th>Temps passé</th>
                    <th>Commune</th>
                    <th>Lieux-dits</th>
                    <th>Type de mission</th>
                    <th>Mission</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?= $MML ?>
                </tbody>
            </table>
        </div>
    </div>

</main>

<?= $renderer->render('footer') ?>

<script type="text/javascript">

    $(document).ready(function () {
        hide_all_select();
        hide_lieux_dits();
    });

    function hide_lieux_dits() {
        <?= $JLIEUDIT ?>
    }

    function call_select(id) {
        hide_all_select();
        $('#' + id).show();
    }

    function hide_all_select() {
        <?= $JMIH ?>
        <?= $JMEH ?>
    }

    <?= $JMEXT ?>
</script>

<script type="application/javascript">

    var locaux = false;
    var binome = false;

    $('#menu').show();
    $('#add_planning').hide();
    $('#my_planning').hide();
    $('#div_locaux').hide();
    $('#div_exterieur').show();
    $('#div_binome').hide();


    function show_menu() {
        $('#menu').show();
        $('#add_planning').hide();
        $('#my_planning').hide();
    }

    function show_add() {
        $('#menu').hide();
        $('#add_planning').show();
        $('#my_planning').hide();
    }

    function show_my() {
        $('#menu').hide();
        $('#add_planning').hide();
        $('#my_planning').show();
    }

    function show_locaux() {
        if (locaux === true) {
            locaux = false;
            $('#div_locaux').hide();
            $('#div_exterieur').show();
        } else {
            locaux = true;
            $('#div_locaux').show();
            $('#div_exterieur').hide();
        }
    }

    function show_binome() {
        if (binome === true) {
            binome = false;
            $('#div_binome').hide();
        } else {
            binome = true;
            $('#div_binome').show();
        }
    }


    function commune(){
        var selectBox = document.getElementById("select_commune");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        hide_lieux_dits();
        call_select('lieu_'+selectedValue);
    }


    $(document).ready(function () {
        var date_input = $('input[name="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
            weekStart: 1,
            orientation: "top",
            language: 'fr'
        };
        date_input.datepicker(options);
    });

    $('#time_debut').timepicker({
        template: 'dropdown',
        appendWidgetTo: 'body',
        maxHours : 24,
        showSeconds : false,
        showMeridian: false,
        secondStep : 5,
        defaultTime : 'current',
        icons : {
            up: '"></span><i class="fas fa-angle-up"></i><span class="',
            down: '"></span><i class="fas fa-angle-down"></i><span class="'
        }
    });

    $('#time_fin').timepicker({
        template: 'dropdown',
        appendWidgetTo: 'body',
        maxHours : 24,
        showSeconds : false,
        showMeridian: false,
        secondStep : 5,
        defaultTime : 'current',
        icons : {
            up: '"></span><i class="fas fa-angle-up"></i><span class="',
            down: '"></span><i class="fas fa-angle-down"></i><span class="'
        }
    });

    $("[name='locaux']").bootstrapSwitch();
    $("[name='binome']").bootstrapSwitch();

</script>