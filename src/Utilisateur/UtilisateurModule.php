<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 15/05/18
 * Time: 13:52
 */

namespace App\Utilisateur;

use Framework\Renderer;
use Framework\Router;
use Framework\Session;

class UtilisateurModule
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * ErrorModule constructor.
     * @param Router $router
     * @param Renderer $renderer
     */
    public function __construct(Router $router, Renderer $renderer)
    {
        // Ajoute le render et le router dans des variable pour les envoyer a des sub Controller
        $this->renderer = $renderer;
        $this->router = $router;


        // Apelle a la sassion
        $info = json_decode((new Session())->GetSession('User'),true);


        // Apelle d'un sub controller
        $controller = new UtilisateurController($this->renderer,$this->router);


        // Ajout du dossier de rendue pour ce module
        $this->renderer->addPath('user', __DIR__ . DIRECTORY_SEPARATOR . 'views');


        // Ajout de variables global
        $this->renderer->addGlobal('nom',$info['nom']);
        $this->renderer->addGlobal('prenom',$info['prenom']);


        // liste des url compatible
        // Methode(URL , [ ClassController , 'NameFunction'], 'NameRoute');
        $router->get($GLOBALS['BaseURL'].'', [$controller, 'Get_index'], 'user.get.index');
        $router->get($GLOBALS['BaseURL'].'mission/{id:[0-9]+}', [$controller, 'Get_mission'], 'user.get.mission');
        $router->get($GLOBALS['BaseURL'].'mission/valide/{id:[0-9]+}', [$controller, 'Get_Validemission'], 'user.get.binome');

        $router->post($GLOBALS['BaseURL'].'ajout/planning', [$controller, 'Post_AddPlanning'], 'user.post.add');
        $router->post($GLOBALS['BaseURL'].'mission/valide/{id:[0-9]+}', [$controller, 'Post_ValideMission'], 'user.post.valide');

        $router->get($GLOBALS['BaseURL'].'logout', [$this, 'Get_logout'], 'login.get.logout');

    }

    /**
     * @return string
     */
    public function Get_logout(): string
    {
        $session = new Session();
        $session->RemSession('User');
        return $this->renderer->render('@error/valide', ['slug' => 'Vous êtes déconnecté', 'url' => $GLOBALS['BaseURL']]);

    }
}