<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 17/05/18
 * Time: 14:09
 */

namespace App\Utilisateur\SubController;

use Framework\Config;
use Framework\Database;
use Framework\Renderer;
use Framework\Router;
use Framework\Session;

class Index
{

    /**
     * call generale pour generer toute les infrmation de l'index
     *
     * @param Renderer $renderer
     * @param Router $router
     * @return string
     */
    public function call(Renderer $renderer, Router $router): string
    {

        $option = $this->AddPlanning($renderer);
        $option['MML'] = $this->ListesMission($router);
        $option['BI'] = $this->MissionBinome($router);
        return $renderer->render('@user/index', $option);

    }

    /**
     * GGeneration de la liste des misison pour l'utilisateur
     *
     * @param Router $router
     * @return string
     */
    private function ListesMission(Router $router): string
    {
        $MML = "";

        $session = new Session();
        $idEcogarde = json_decode($session->GetSession('User'), true)['id'];

        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('mission')." WHERE id_ecogarde = ? OR binome = ?;", [$idEcogarde, $idEcogarde], true);

        $binomeListe = [];

        foreach ($retour as $value) {
            if ($value['binome'] !== -1) {
                if (!isset($binomeListe[$value['binome']])) {
                    $info = $database->SQL("SELECT nom, prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$value['binome']], true);
                    $binome = $info[0]['nom']." ".$info[0]['prenom'];
                    $binomeListe[$value['binome']] = $binome;
                } else {
                    $binome = $binomeListe[$value['binome']];
                }
            } else {
                $binome = "Seul";
            }

            $commune = "";
            $lieuxDit = "";

            $config = new Config('Commune.json');

            if ($config->GetConfig($value['commune']) !== false) {
                $info = $config->GetConfig();
                foreach ($info as $forKey => $forValue) {
                    if ($forValue['Code'] === $value['commune']) {
                        $commune = $forKey;
                        $lieuxDit = $forValue['LieuDit'][$value['lieux_dit']];
                    }
                }
            }

            $configCode = new Config("Code.json");
            $retour = $configCode->GetConfig('Code_Mission');
            $configCode = $retour;

            // Verification mission administrative ou exterireur
            if ($value['mission_ext']) {
                $mission_type = "Mission de terain";
                $configCode = $configCode['MissionExt'];
                $option = "Ext_";
            } else {
                $mission_type = "Mission administratif";
                $configCode = $configCode['MissionInt'];
                $option = "Int_";
            }

            $name = $mission_type." : ".($configCode[$value['mission_code']]);

            $MML = $MML."
            <tr>
                <td>".$value['id']."</td>
                <td>".$binome."</td>
                <td>".$value['date']."</td>
                <td>".$value['temps']."</td>
                <td>".$commune."</td>
                <td>".$lieuxDit."</td>
                <td>".$name."</td>
                <td><a href='".$router->generateUri("user.get.binome", ['id' => $value['id']])."' class=\"badge badge-success\">Valider</a></td>
            </tr>";

        }

        return $MML;
    }

    /**
     * Generation de l'ajout de planning
     *
     * @param Renderer $renderer
     * @return array
     */
    private function AddPlanning(Renderer $renderer): array
    {
        $mission = new Config("Mission.json");
        $missionExt = $mission->GetConfig("MissionExt");
        $missionInt = $mission->GetConfig("MissionInt");

        $codeConfig = (new Config("Code.json"))->GetConfig("Mission_Code");
        $codeInt = $codeConfig["MissionInt"];
        $codeExt = $codeConfig["MissionExt"];

        $tables = new Config("Tables.json");

        $html_mission_int_liste = "";
        $html_mission_int_select = "";
        $js_mission_int_hide = "";
        $html_mission_int_champ = "";
        $js_mission_extension = "";

        foreach ($missionInt as $key => $value) {

            $code = $codeInt[$key];
            $name = $key;
            $html_mission_int_liste = $html_mission_int_liste."<div class=\"custom-control custom-radio\"><input type=\"radio\" id=\"".$code."\" name=\"check\" value='".$code."' class=\"custom-control-input\"><label class=\"custom-control-label\" for=\"".$code."\" onclick=\"call_select('div_".$code."');\">".$name."</label></div>";
            if (!empty($value)) {
                $option = "";
                foreach ($value as $id => $nom) {
                    $option = $option."<option value=\"".$id."\">".$nom."</option>";
                }
                $html_mission_int_select = $html_mission_int_select."<div id=\"div_".$code."\"><div class=\"form-group\"><label for=\"select_".$code."\">".$name."</label><select name=\"select_".$code."\" class=\"custom-select\" id=\"select_".$code."\"><option value=\"0\">Choisir...</option>".$option."</select></div></div>";
            }
            $js_mission_int_hide = $js_mission_int_hide."$('#div_".$code."').hide();";
        }

        $html_mission_ext_liste = "";
        $html_mission_ext_select = "";
        $html_mission_ext_champ = "";
        $js_mission_ext_hide = "";

        $database = new Database();
        $retour = $database->SQL("SELECT id FROM ".$database->Schemas('mission')." where id = (select max(id) from ecogarde.mission);", [], true);

        if (empty($retour)) {
            $missionId = 1;
        } else {
            $missionId = intval($retour[0]['id']) + 1;
        }

        $session = new Session();
        $retour = json_decode($session->GetSession('User'), true);

        $ecogardeId = $retour['id'];

        foreach ($missionExt as $key => $value) {
            $code = $codeExt[$key];
            $name = $key;
            $extension = "";
            $tablesListe = $tables->GetConfig($code);

            if ($tablesListe === $code) {
                $tablesInfo = $tables->GetConfig("Ext_".$name)['Information'];

                $js_mission_extension = $js_mission_extension."function show_all_extension_".$code."(){";

                $extension = "show_all_extension_".$code."();";

                foreach ($tablesInfo as $tablesInfoKey => $tablesInfoValue) {

                    if ($tablesInfoKey === 'Option') {

                    } else {

                        if (isset($tablesInfoValue['Afficher'])) {
                            $champName = $tablesInfoValue['Afficher'];
                        } else {
                            $champName = $tablesInfoValue['Nom'];
                        }

                        $champType = $tablesInfoValue['Type'];
                        $champCode = $code."_".crc32($champName);

                        $js_mission_ext_hide = $js_mission_ext_hide."$('#champ_".$champCode."').hide();";

                        $js_mission_extension = $js_mission_extension."$('#champ_".$champCode."').show();";

                        if (is_array($tablesInfoValue['Type'])) {

                            if ($tablesInfoValue['Type']['Option'] === 'REF') {

                                $ref = $tablesInfoValue['Type']['REF'];

                                if (isset($ref['Tables'])) {
                                    if ($ref['Tables'] === "Mission") {
                                        $html_mission_ext_champ = $html_mission_ext_champ."<input type=\"hidden\" name=\"champ_".$champCode."_id_mission\" id=\"champ_".$champCode."_id_mission\" value=\"".$missionId."\">";
                                    } elseif ($ref['Tables'] === "Ecogarde") {
                                        $html_mission_ext_champ = $html_mission_ext_champ."<input type=\"hidden\" name=\"champ_".$champCode."_id_ecogarde\" id=\"champ_".$champCode."_id_ecogarde\" value=\"".$ecogardeId."\">";
                                    }
                                }
                            }
                        }

                        if ($champType === 'int') {
                            $html_mission_ext_champ = $html_mission_ext_champ."<div class=\"form-group\" id='champ_".$champCode."'><label for=\"champ_".$champCode."_input\">".$champName."</label><input type=\"number\" class=\"form-control\" id=\"champ_".$champCode."_input\" name=\"champ_".$champCode."_input\"></div>";
                        } elseif ($champType === 'text') {
                            $html_mission_ext_champ = $html_mission_ext_champ."<div class=\"form-group\" id='champ_".$champCode."'><label for=\"champ_".$champCode."_input\">".$champName."</label><textarea class=\"form-control\" id=\"champ_".$champCode."_input\" rows=\"3\" maxlength=\"500\" name=\"champ_".$champCode."_input\"></textarea></div>";
                        } elseif ($champType === 'LIST') {

                            $option = "";

                            foreach ($tablesInfoValue['LIST'] as $listeKey => $listeValue) {
                                $option = $option."<option value=\"".$listeValue."\">".$listeValue."</option>";
                            }

                            $html_mission_ext_champ = $html_mission_ext_champ."<div id='champ_".$champCode."'><div class=\"form-group\"><label for=\"champ_".$champCode."_input\">".$champName."</label><select id=\"champ_".$champCode."_input\" class=\"custom-select\" name=\"champ_".$champCode."_input\"><option value=\"0\" selected>Choisir...</option>".$option."</select></div></div>";
                        }

                    }
                }

                $js_mission_extension = $js_mission_extension."}";

            }

            $html_mission_ext_liste = $html_mission_ext_liste."<div class=\"custom-control custom-radio\"><input type=\"radio\" id=\"".$code."\" name=\"check\" value='".$code."' class=\"custom-control-input\"><label class=\"custom-control-label\" for=\"".$code."\" onclick=\"call_select('div_".$code."');".$extension."\">".$name."</label></div>";
            if (!empty($value)) {
                $option = "";
                foreach ($value as $id => $nom) {
                    $option = $option."<option value=\"".$id."\">".$nom."</option>";
                }
                $html_mission_ext_select = $html_mission_ext_select."<div id=\"div_".$code."\"><div class=\"form-group\"><label for=\"select_".$code."\">Option</label><select name=\"select_".$code."\" class=\"custom-select\" id=\"select_".$code."\"><option value=\"0\" selected>Choisir...</option>".$option."</select></div></div>";
            }
            $js_mission_ext_hide = $js_mission_ext_hide."$('#div_".$code."').hide();";
        }

        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." where id != ?;", [$ecogardeId], true);
        $option = "";

        foreach ($retour as $key => $value) {
            $option = $option."<option value=\"".$value['id']."\">".$value['nom']." ".$value['prenom']."</option>";
        }

        $html_binome = "<div class=\"form-group\"><label for=\"select_binome\">Selection d'un binome</label><select class=\"form-control\" id=\"select_binome\" name='select_binome'><option value=\"-1\" selected>Choisir...</option>".$option."</select></div>";

        $html_lieu_dit_liste = "";
        $optionCommune = "";
        $js_lieu_dit = "";

        $config = new Config('Commune.json');
        $retour = $config->GetConfig();

        foreach ($retour as $key => $value) {
            $optionCommune = $optionCommune."<option value=\"".$value['Code']."\">".$key."</option>";

            $optionLieu = "";

            foreach ($value['LieuDit'] as $lieuKey => $lieuValue) {
                $optionLieu = $optionLieu."<option value=\"".$lieuKey."\">".$lieuValue."</option>";
            }

            $html_lieu_dit_liste = $html_lieu_dit_liste."<div class=\"form-group\" id='lieu_".$value['Code']."'><label for=\"select_lieu_".$value['Code']."\">Lieu Dit pour la commune : ".$key."</label><select class=\"form-control\" id=\"select_lieu_".$value['Code']."\" name='select_lieu_".$value['Code']."'><option value=\"\" selected>Choisir...</option>".$optionLieu."</select></div>";

            $js_lieu_dit = $js_lieu_dit."$('#lieu_".$value['Code']."').hide();";

        }

        $html_commune_liste = "<div class=\"form-group\"><label for=\"select_commune\">Commune</label><select class=\"form-control\" id=\"select_commune\" name='select_commune' onchange=\"commune();\" required><option value=\"-1\" selected>Choisir...</option>".$optionCommune."</select></div>";

        return [
            'MIL' => $html_mission_int_liste,
            'MIS' => $html_mission_int_select,
            'MIC' => $html_mission_int_champ,
            'JMIH' => $js_mission_int_hide,
            'MEL' => $html_mission_ext_liste,
            'MES' => $html_mission_ext_select,
            'MEC' => $html_mission_ext_champ,
            'JMEH' => $js_mission_ext_hide,
            'JMEXT' => $js_mission_extension,
            'HB' => $html_binome,
            'HCL' => $html_commune_liste,
            'HLDL' => $html_lieu_dit_liste,
            'JLIEUDIT' => $js_lieu_dit
        ];
    }

    /**
     * Liste les mission non valider par les binome
     *
     * @param $router
     * @return string
     */
    private function MissionBinome(Router $router): string
    {
        $database = new Database();
        $session = new Session();
        $session = json_decode($session->GetSession('User'), true);

        $retour = $database->SQL("SELECT id,id_ecogarde,binome,date,temps,commune,lieux_dit,mission_code,mission_int,mission_ext from ".$database->Schemas('mission')." WHERE binome_valide = false AND binome = ?;", [$session['id']], true);

        $option = "";
        $binomeListe = [];

        if (!empty($retour)) {

            foreach ($retour as $key => $value) {

                if ($value['binome'] !== -1) {
                    if (!isset($binomeListe[$value['binome']])) {
                        $info = $database->SQL("SELECT nom, prenom FROM ".$database->Schemas('ecogardes')." WHERE id = ?;", [$value['id_ecogarde']], true);
                        $binome = $info[0]['nom']." ".$info[0]['prenom'];
                        $binomeListe[$value['binome']] = $binome;
                    } else {
                        $binome = $binomeListe[$value['binome']];
                    }
                } else {
                    $binome = "Seul";
                }

                $commune = "";
                $lieuxDit = "";
                $type = "";

                $config = new Config('Commune.json');

                if ($config->GetConfig($value['commune']) !== false) {
                    $info = $config->GetConfig();
                    foreach ($info as $forKey => $forValue) {
                        if ($forValue['Code'] === $value['commune']) {
                            $commune = $forKey;
                            $lieuxDit = $forValue['LieuDit'][$value['lieux_dit']];
                        }
                    }
                }

                $configCode = new Config("Code.json");
                $retour = $configCode->GetConfig('Code_Mission');
                $configCode = $retour;

                // Verification mission administrative ou exterireur
                if ($value['mission_ext']) {
                    $mission_type = "Mission de terain";
                    $configCode = $configCode['MissionExt'];
                    $option = "Ext_";
                } else {
                    $mission_type = "Mission administratif";
                    $configCode = $configCode['MissionInt'];
                    $option = "Int_";
                }

                $name = $mission_type." : ".($configCode[$value['mission_code']]);

                $option = $option."
            <tr>
                <td>".$value['id']."</td>
                <td>".$binome."</td>
                <td>".$value['date']."</td>
                <td>".$value['temps']."</td>
                <td>".$commune."</td>
                <td>".$lieuxDit."</td>
                <td>".$name."</td>
                <td><a href='".$router->generateUri("user.get.binome", ['id' => $value['id']])."' class=\"badge badge-success\">Valider</a></td>
            </tr>";
            }

            $tables = "
        <div class=\"album py-5 bg-light\">
            <div class=\"container\" style=\"text-align: center;\">
            
            <h3>Demmande de validation par un binome.</h3>
            <br>
            
            <table id=\"tables_BI\" class=\"display table\" style=\"width:100%\">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Binome</th>
                    <th>Date</th>
                    <th>Temps passé</th>
                    <th>Commune</th>
                    <th>Lieux Dit</th>
                    <th>Mission</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                
                ".$option."
                
                </tbody>
            </table>
                        </div>
        </div>
";

            return $tables;

        } else {
            return "";
        }

    }

}