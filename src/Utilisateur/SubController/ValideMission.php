<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/06/18
 * Time: 13:17
 */

namespace App\Utilisateur\SubController;

use Framework\Config;
use Framework\Database;
use Framework\Renderer;
use Framework\Router;
use GuzzleHttp\Psr7\ServerRequest;

class ValideMission
{
    /**
     * Apelle generale pour la generation
     *
     * @param Renderer $renderer
     * @param Router $router
     * @param ServerRequest $request
     * @return string
     */
    public function call(Renderer $renderer, Router $router, ServerRequest $request): string
    {

        $id = $request->getAttribute('id');

        $getmission = (new GetMission())->Generator($renderer, $router, $request);
        $affichage = $getmission."<br><br>
            <form method='post' action='".$router->generateUri("user.post.valide", ['id' => $id])."'>
                <button type='submit' class='btn btn-outline-success btn-block btn-lg'>Valider les informations</button>
            </form>";
        return $renderer->render('@user/ValideMission', ['Affichage' => $affichage]);

    }

    /**
     * Apelle lors de la validation d'un binome
     *
     * @param Renderer $renderer
     * @param Router $router
     * @param ServerRequest $request
     * @return string
     */
    public function post(Renderer $renderer, Router $router, ServerRequest $request): string
    {
        $id = $request->getAttribute('id');

        $database = new Database();
        $database->SQL("UPDATE ".$database->Schemas('mission')." SET binome_valide = true WHERE binome_valide = false AND id = ?;", [$id], false);

        return $renderer->render('@error/valide', ['slug' => "Mission Valider"]);
    }
}