<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 17/05/18
 * Time: 14:09
 */

namespace App\Utilisateur\SubController;

use Framework\Config;
use Framework\Database;
use Framework\Renderer;
use Framework\Session;

class AddPlanning
{

    /**
     * Finction d'ajout de misison dans la DB
     *
     * @param Renderer $renderer
     * @return string
     */
    public function call(Renderer $renderer): string
    {
        if (!isset($_POST)) {
            return $renderer->render('@error/error', ["slug" => "Erreur lors de l'envoi"]);
        }

        $locaux = false;
        $binome = -1;

        if (isset($_POST['binome'])) {
            if ($_POST['binome'] === 'on') {
                $binome = $_POST['select_binome'];
            }
        }

        if (isset($_POST['locaux'])) {
            $locaux = true;
        }

        $error = "";

        if (!isset($_POST['time_debut'])) {
            $error = $error."Erreur, car vous n'avez pas enter l'heur de début.<br>";
        }

        if (!isset($_POST['time_fin'])) {
            $error = $error."Erreur, car vous n'avez pas enter l'heur de fin.<br>";
        }

        if (!isset($_POST['date'])) {
            $error = $error."Erreur, car vous n'avez pas enter la date.<br>";
        }

        if (!isset($_POST['select_commune'])) {
            $error = $error."Erreur, car vous n'avez pas enter de commune.<br>";
        } else {
            if ($_POST['select_commune'] === '-1' && $locaux !== true) {
                $error = $error."Erreur, car vous n'avez pas enter de commune.<br>";
            }
        }

        if (!isset($_POST['select_lieu_'.$_POST['select_commune']]) && $locaux !== true) {
            $error = $error."Erreur, car vous n'avez pas enter de lieux dit.<br>";
        }

        if (!isset($_POST['check']) || $error !== "") {
            return $renderer->render('@error/error', ["slug" => $error]);
        }

        $date = $_POST['date'];
        $start = $_POST['time_debut'];
        $end = $_POST['time_fin'];
        $remarque = $_POST['remarque'];

        if ($locaux === true){
            if (!isset($_POST['select_lieu_'.$_POST['select_commune']])){
                $commune = '-1';
                $lieudit = '-1';
            }else{
                $commune = $_POST['select_commune'];
                $lieudit = $_POST['select_lieu_'.$_POST['select_commune']];
            }
        }else{
            $commune = $_POST['select_commune'];
            $lieudit = $_POST['select_lieu_'.$_POST['select_commune']];
        }

        $type = $_POST['check'];
        $option = -1;

        if (isset($_POST['select_'.$type])) {
            $option = $_POST['select_'.$type];
        }


        $mission = new Config("Mission.json");

        if ($locaux === true) {
            $missionInfo = $mission->GetConfig("MissionInt");
        } else {
            $missionInfo = $mission->GetConfig("MissionExt");
        }

        foreach ($missionInfo as $key => $value) {
            $code = crc32($key);
            if ($code === intval($type)) {
                $type = $key;
                if ($option !== -1) {
                    $option = $value[$option];
                }
                break;
            }
        }


        $start = explode(':', $start);
        $end = explode(':', $end);

        $time[0] = (intval($end[0]) - intval($start[0]));
        $time[1] = (intval($end[1]) - intval($start[1]));

        if ($time[1] < -0) {
            $time[1] = $time[1] + (2 * -$time[1]);
        }

        if (($time[0] < -0) || ($time[0] == 0 && $time[1] == 0)) {
            return $renderer->render('@error/error', ["slug" => "Erreur, vous avez entre une heure incorrecte ou négative."]);
        }

        if ($time[0] < 10) {
            $time[0] = '0'.$time[0];
        }

        if ($time[1] < 10) {
            $time[1] = '0'.$time[1];
        }

        $time = join("h", $time);


        $info = (['Date' => $date, 'Time' => $time, 'Binome' => $binome, 'Commune' => $commune, 'LieuDit' => $lieudit, 'Remarque' => $remarque, 'Locaux' => $locaux, 'Type' => $type, 'Option' => $option]);

        $forCode = explode('_', $type)[1];

        if ($locaux === true) {
            $forType = "INT";

        } else {
            $forType = "EXT";
        }

        $plus = [];

        foreach ($_POST as $key => $value) {
            $forArray = explode('_', $key);

            if ($forArray[0] === "champ" && $forArray[1] === $forType && $forArray[2] === $forCode) {
                if (in_array('mission', $forArray) === false AND in_array('ecogarde', $forArray) === false) {
                    array_push($plus, join("_", $forArray));
                }
            }

        }

        if (!empty($plus)) {

            $configCode = new Config("Code.json");
            $configTables = new Config("Tables.json");

            if ($locaux === true) {
                $Code = $configCode->GetConfig("Code_Mission")['MissionInt'];
                $Tables = $configTables->GetConfig('Int_'.$Code[$type])['Information'];
                $Name = 'Int_'.$Code[$type];
            } else {
                $Code = $configCode->GetConfig("Code_Mission")['MissionExt'];
                $Tables = $configTables->GetConfig('Ext_'.$Code[$type])['Information'];
                $Name = 'Ext_'.$Code[$type];
            }

            $id = 0;

            foreach ($Tables as $key => $value) {
                if (crc32($value['Nom']) == explode("_", $plus[$id])[3]) {
                    $plus[$value['Nom']] = $plus[$id];
                    unset($plus[$id]);
                    $id = $id + 1;
                }
            }

            foreach ($plus as $key => $value) {

                if (isset($_POST[$value])) {
                    $plus[$key] = $_POST[$value];
                }
            }

            $plus['TableName'] = $Name;

            $info['Extension'] = $plus;
        }

        $session = new Session();
        $idEcogarde = json_decode($session->GetSession('User'), true)['id'];

        $database = new Database();
        $retour = $database->SQL("SELECT date, temps, commune, lieux_dit, mission_code FROM ".$database->Schemas('mission')." WHERE temps = ? AND date = ?;", [$time, $date], true);

        if (!empty($retour)) {
            foreach ($retour as $key => $value) {
                if ($value['lieux_dit'] === $info['LieuDit'] && $value['mission_code'] === $info['Type'] && $value['commune'] === $info['Commune'] && $value['date'] === $info['date']) {
                    return $renderer->render('@error/error', ["slug" => "Erreur, vous avez entre un double, merci de verifier les informations."]);
                }
            }
        }

        $retour = $database->SQL("SELECT id FROM ".$database->Schemas('mission')." where id = (select max(id) from ".$database->Schemas('mission').");", [], true);

        if (empty($retour)) {
            $missionId = 1;
        } else {
            $missionId = intval($retour[0]['id']) + 1;
        }

        if ($info['Binome'] === -1) {
            if ($info['Locaux'] === true) {
                $sql = "INSERT INTO ".$database->Schemas('mission')." VALUES (?,?,?,?,?,?,?,true,false,?,?,?,true);";
            } else {
                $sql = "INSERT INTO ".$database->Schemas('mission')." VALUES (?,?,?,?,?,?,?,false,true,?,?,?,true);";
            }
        } else {

            if ($info['Locaux'] === true) {
                $sql = "INSERT INTO ".$database->Schemas('mission')." VALUES (?,?,?,?,?,?,?,true,false,?,?,?,false);";
            } else {
                $sql = "INSERT INTO ".$database->Schemas('mission')." VALUES (?,?,?,?,?,?,?,false,true,?,?,?,false);";
            }
        }

        $database->SQL($sql, [
            $missionId,
            $idEcogarde,
            $info['Binome'],
            $info['Date'],
            $info['Time'],
            $info['Commune'],
            $info['LieuDit'],
            $info['Type'],
            $info['Option'],
            $info['Remarque']
        ], false);

        if (isset($info['Extension'])) {
            $table = strtolower($info['Extension']['TableName']);
            unset($info['Extension']['TableName']);

            $count = count($info['Extension']);
            $i = 0;
            $char = "?,?,";
            while ($i <= $count) {
                $char = $char."?";
                if (($i + 1) <= $count) {
                    $char = $char.",";
                }
                $i = $i + 1;
            }

            $sql = "INSERT INTO ".$database->Schemas($table)." VALUES (".$char.");";

            $retour = $database->SQL("SELECT id FROM ".$database->Schemas($table)." where id = (select max(id) from ".$database->Schemas($table).");", [], true);

            if (empty($retour)) {
                $ExtensionId = 1;
            } else {
                $ExtensionId = $retour[count($retour) - 1]['id'] + 1;
            }

            $sqlInfo = [];

            array_push($sqlInfo, $ExtensionId);
            array_push($sqlInfo, $missionId);
            array_push($sqlInfo, $idEcogarde);

            foreach ($info['Extension'] as $value) {
                array_push($sqlInfo, $value);
            }

            $database->SQL($sql, $sqlInfo, false);
        }
        return $renderer->render('@error/valide', ["slug" => "Toutes les informations on été ajouté."]);

    }

}