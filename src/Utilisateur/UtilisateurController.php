<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 15/05/18
 * Time: 13:53
 */

namespace App\Utilisateur;

use App\Utilisateur\SubController\AddPlanning;
use App\Utilisateur\SubController\GetMission;
use App\Utilisateur\SubController\Index;
use App\Utilisateur\SubController\ValideMission;
use Framework\Renderer;
use Framework\Router;
use GuzzleHttp\Psr7\ServerRequest;

class UtilisateurController
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * UtilisateurController constructor.
     * @param Renderer $renderer
     * @param Router $router
     */
    public function __construct(Renderer $renderer, Router $router)
    {
        $this->renderer = $renderer;
        $this->router = $router;
    }

    /**
     * @param ServerRequest $request
     * @return string
     */
    public function Get_index(ServerRequest $request): string
    {
        return (new Index())->call($this->renderer, $this->router);
    }

    /**
     * @param ServerRequest $request
     * @return string
     */
    public function Post_AddPlanning(ServerRequest $request): string
    {
        return (new AddPlanning())->call($this->renderer);
    }

    /**
     * @param ServerRequest $request
     * @return string
     */
    public function Post_ValideMission(ServerRequest $request): string
    {
        return (new ValideMission())->post($this->renderer, $this->router, $request);
    }

    /**
     * @param ServerRequest $request
     * @return string
     */
    public function Get_Validemission(ServerRequest $request): string
    {
        return (new ValideMission())->call($this->renderer, $this->router, $request);
    }

    /**
     * @param ServerRequest $request
     * @return string
     */
    public function Get_mission(ServerRequest $request): string
    {
        return (new GetMission())->call($this->renderer, $this->router, $request);
    }
}