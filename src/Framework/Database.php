<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/05/18
 * Time: 09:14
 */

namespace Framework;

use PDO;
use PDOException;

class Database
{

    private $dbh;
    private $schemats;

    /**
     * Database constructor.
     * @param string $Database_Name
     */
    public function __construct(string $Database_Name = "")
    {

        $config = new Config();
        $info = $config->GetConfig("Database");

        if ($info === false) {
            print_r("Error!: Configuration de la database non valide !");
            die();
        } else {

            $type = $info['Database_Type'];
            $host = $info['Database_Host'];
            $login = $info['Database_Login'];
            $pass = $info['Database_Pass'];
            $port = $info['Database_Port'];

            if ($Database_Name === ""){
                $name = $info['Database_Name'];
            } else {
                $name = $Database_Name;
            };


            if ($info['Database_Schemas'] !== ""){
                $this->schemats = $info['Database_Schemas'];
            } else {
                $this->schemats = "";
            };
        }

        try {
            $this->dbh = new PDO($type.':host='.$host.';port='.$port.';dbname='.$name, $login, $pass);
        } catch (PDOException $e) {
            print "Error DB : ".$e->getMessage()."<br/>";
            die();
        }
    }

    /**
     * Permet d'excuter du SQL en toute securiter
     * @param String $sql
     * @param array $argument
     * @param bool $return
     * @return array
     */
    public function SQL(String $sql, Array $argument, bool $return = false): array
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($argument);
        if ($return !== false) {
            return $sth->fetchAll();
        }
        return [];
    }

    /**
     * permet de gerer les schemas postgres
     * @param string $talbes
     * @return string
     */
    public function Schemas(string $talbes): string
    {
        if ($this->schemats !== ""){
            return $this->schemats.".".$talbes;
        }else{
            return $talbes;
        }
    }

}