<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/05/18
 * Time: 09:39
 */

namespace Framework;


class Session
{

    private $Session;
    private $encrypt;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        $config = new Config();
        $key = $config->GetConfig("Security_Key");

        $this->encrypt = new Encrypt($key);
        $this->Session = $_SESSION;
    }

    /**
     * Creer une entre dans la session
     * @param string $name
     * @param string $var
     * @param bool $redefine
     * @return bool
     */
    public function SetSession(string $name, string $var, bool $redefine = false): bool
    {
        if ($redefine !== false) {
            try {
                $varEncrypt = $this->encrypt->Encrypt($var);
            } catch (\Exception $e) {
                var_dump($e);
                die();
            }
            $_SESSION[$name] = $varEncrypt;
        } else {
            if (isset($_SESSION[$name]) === true) {
                return false;
            } else {
                try {
                    $varEncrypt = $this->encrypt->Encrypt($var);
                } catch (\Exception $e) {
                    var_dump($e);
                    die();
                }
                $_SESSION[$name] = $varEncrypt;
            }
        }
        return true;
    }

    /**
     * Get un pertie de la session
     * @param string $name
     * @return string
     */
    public function GetSession(string $name): string
    {
        if (isset($_SESSION)){
            if (isset($_SESSION[$name])){
                try {
                    return $this->encrypt->Decrypt($_SESSION[$name]);
                } catch (\Exception $e) {
                    var_dump($e);
                    die();
                }
            }
        }
        return false;
    }

    /**
     * Supprime la session
     * @param string $name
     */
    public function RemSession(string $name)
    {
        $_SESSION[$name] = null;
        session_destroy();
    }

}