<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 21/01/2018
 * Time: 11:01
 */

namespace Framework;

use Framework\Router\Route;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\Route as ZendRoute;

/**
 * Class Router
 * Road Systems
 *
 * @package Framework
 */
class Router
{
    /**
     * @var \Zend\Expressive\Router\FastRouteRouter
     */
    private $router;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->router = new FastRouteRouter();
    }

    /**
     * Create the route in get
     *
     * @param string   $path
     * @param callable $callable
     * @param string   $name
     */
    public function get(string $path, callable $callable, string $name)
    {
        $this->router->addRoute(new ZendRoute($path, $callable, ['GET'], $name));
    }


    /**
     * Create the route in post
     *
     * @param string   $path
     * @param callable $callable
     * @param string   $name
     */
    public function post(string $path, callable $callable, string $name)
    {
        $this->router->addRoute(new ZendRoute($path, $callable, ['POST'], $name));
    }

    /**
     * Pick up the road
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return \Framework\Router\Route|null
     */
    public function match(ServerRequestInterface $request): ?Route
    {
        $result = $this->router->match($request);
        if ($result->isSuccess() === true) {
            return new Route(
                $result->getMatchedRouteName(),
                $result->getMatchedMiddleware(),
                $result->getMatchedParams()
            );
        }

        return null;
    }

    /**
     * Create a url with his name
     *
     * @param string $name
     * @param array  $params
     *
     * @return null|string
     */
    public function generateUri(string $name, array $params): ?string
    {
        return $this->router->generateUri($name, $params);
    }
}
