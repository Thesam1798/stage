<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 21/01/2018
 * Time: 11:04
 */

namespace Framework\Router;

/**
 * Class Route
 * Retrieve all information related to the route
 *
 * @package Framework\Router
 */
class Route
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var callable
     */
    private $callback;
    /**
     * @var string[]
     */
    private $parameters;

    public function __construct(string $name, callable $callback, array $parameters)
    {
        $this->name = $name;
        $this->callback = $callback;
        $this->parameters = $parameters;
    }

    /**
     * Get the name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Retrieves the functions to be performed
     *
     * @return callable
     */
    public function getCallback(): callable
    {
        return $this->callback;
    }


    /**
     * Retrieves information passes through the url
     *
     * @return string[]
     */
    public function getParams(): array
    {
        return $this->parameters;
    }
}
