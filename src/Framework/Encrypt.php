<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/05/18
 * Time: 09:42
 */

namespace Framework;

use Exception;
use RangeException;

class Encrypt
{

    /**
     * @var string
     */
    private $key;

    /**
     * Encrypt constructor.
     * @param string $key - encryption key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param string $message
     * @return string
     */
    public function Encrypt(string $message):string
    {
        return $this->encrypt_decrypt('encrypt',$message);
    }

    /**
     * @param string $message
     * @return string
     */
    public function Decrypt(string $message):string
    {
        return $this->encrypt_decrypt('decrypt',$message);
    }

    /**
     * Permet de crypter ou decrypter des information en claire
     * @param $action
     * @param $string
     * @return bool|string
     */
    private function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = $this->key;
        $secret_iv = $this->key;
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

}