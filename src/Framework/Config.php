<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/05/18
 * Time: 10:36
 */

namespace Framework;


class Config
{
    /**
     * @var mixed
     */
    private $config;

    /**
     * Config constructor, Permet de recuperer les config d'un json.
     * @param string $file
     * @param string $folder
     */
    public function __construct(string $file = "Master.json", string $folder = "")
    {
        if ($folder === "") {
            $folder = (dirname(dirname(__DIR__))."/config/");
        }

	$texte = file_get_contents($folder.$file);

        $this->config = json_decode($texte, true);
    }

    /**
     * Recupert une config ou une variable.
     * @param string $name
     * @return bool|mixed
     */
    public function GetConfig(string $name = "")
    {
        if ($name === "") {
            return $this->config;
        } else {
            foreach ($this->config as $key => $value) {
                if ($key === $name || $value === $name) {
                    return $value;
                }
                if (is_array($value)) {
                    $inside = $this->inArray($value,$name);
                    if ($inside !== false){
                        return $inside;
                    }
                }
            }
            return false;
        }
    }

    private function inArray(array $array, string $find)
    {
        foreach ($array as $arrayKey => $arrayValue) {
            if ($arrayKey === $find || $arrayValue === $find) {
                return $arrayValue;
            }
            if (is_array($arrayValue)) {
                $inside = $this->inArray($arrayValue,$find);
                if ($inside !== false){
                    return $inside;
                }
            }
        }
        return false;
    }

}