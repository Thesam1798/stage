<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 04/05/18
 * Time: 08:44
 */

namespace App\Home;

use Framework\Database;
use Framework\Router;
use Framework\Session;
use Framework\Renderer;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface as Request;

class HomeController
{

    private $renderer;
    /**
     * @var Router
     */
    private $router;

    /**
     * HomeController constructor.
     * @param Renderer $renderer
     * @param Router $router
     */
    public function __construct(Renderer $renderer, Router $router)
    {

        $this->renderer = $renderer;
        $this->router = $router;
    }

    /**
     * Affichage de l'index
     *
     * @param Request $request
     * @return string
     */
    public function Get_index(Request $request): string
    {
        return $this->renderer->render('@home/index', []);
    }

    /**
     * Verification des information de login
     *
     * @param Request $request
     * @return string
     */
    public function Post_index(Request $request): string
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $username = explode(".", $username);

        if (isset($username[0]) === true AND isset($username[1]) === true ){
            $database = new Database();
            $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." WHERE nom = ? AND prenom = ?", [strtolower($username[0]), strtolower($username[1])], true);

            if (empty($retour)) {
                return $this->renderer->render('@error/error', ['slug' => 'Imposible de trouver : '.$username[0].' '.$username[1], 'url' => $GLOBALS['BaseURL']]);
            }
        }else{
            return $this->renderer->render('@error/error', ['slug' => 'Imposible de trouver : '.$username[0], 'url' => $GLOBALS['BaseURL']]);
        }

        if ($retour[0]['password_modif'] === true) {
            return $this->renderer->render('@home/password', ['id' => $retour[0]['id']]);
        }

        $verif = password_verify($password, $retour[0]['password']);

        if ($verif !== true) {
            return $this->renderer->render('@error/error', ['slug' => 'Mot de passe incorrect !', 'url' => $GLOBALS['BaseURL']]);
        }

        $info = [];

        foreach ($retour[0] as $key => $value) {
            if (!is_int($key)) {
                $info[$key] = $value;
            }
        }

        $info_json = json_encode($info);

        $session = new Session();
        $session->SetSession('User', $info_json, true);

        if (isset($retour[0]['admin'])) {
            if ($retour[0]['admin'] !== false) {
                $session->SetSession('Admin', json_encode(true), true);
            }
        }

        return $this->renderer->render('@error/valide', ['slug' => 'Connexion en cours', 'url' => $GLOBALS['BaseURL']]);

    }

    /**
     * Modification du mot de passe
     *
     * @param ServerRequest $request
     * @return string
     */
    public function Post_password(ServerRequest $request): string
    {
        $id = $request->getAttribute('id');
        $username = explode('.', $_POST['username']);
        $password = $_POST['password'];
        $password_verif = $_POST['password_verif'];

        $password = password_hash($password, PASSWORD_DEFAULT);

        if (password_verify($password_verif, $password) !== true) {
            return $this->renderer->render('@error/error', ['slug' => 'Mot de passe incorrect lors de la verification !', 'url' => $GLOBALS['BaseURL']]);
        }

        $database = new Database();
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." WHERE nom = ? AND prenom = ?", [strtolower($username[0]), strtolower($username[1])], true);

        if (empty($retour)) {
            return $this->renderer->render('@error/error', ['slug' => 'Imposible de trouver : '.$username[0].' '.$username[1], 'url' => $GLOBALS['BaseURL']]);
        }

        $database->SQL("UPDATE ".$database->Schemas('ecogardes')." SET password = ?, password_modif = false WHERE id = ? AND nom = ? AND prenom = ?;", [$password, $id, $username[0], $username[1]], false);
        $retour = $database->SQL("SELECT * FROM ".$database->Schemas('ecogardes')." WHERE nom = ? AND prenom = ?", [strtolower($username[0]), strtolower($username[1])], true);

        if (empty($retour)) {
            return $this->renderer->render('@error/error', ['slug' => 'Imposible de trouver : '.$username[0].' '.$username[1], 'url' => $GLOBALS['BaseURL']]);
        }

        if ($retour[0]['password_modif'] === true) {
            return $this->renderer->render('@home/password', ['id' => $retour[0]['id']]);
        }

        $verif = password_verify($password_verif, $retour[0]['password']);

        if ($verif !== true) {
            return $this->renderer->render('@error/error', ['slug' => 'Mot de passe incorrect !', 'url' => $GLOBALS['BaseURL']]);
        }

        $info = [];

        foreach ($retour[0] as $key => $value) {
            if (!is_int($key)) {
                $info[$key] = $value;
            }
        }

        $info_json = json_encode($info);

        $session = new Session();
        $session->SetSession('User', $info_json, true);

        if (isset($retour[0]['admin'])) {
            if ($retour[0]['admin'] !== false) {
                $session->SetSession('Admin', json_encode(true), true);
            }
        }

        return $this->renderer->render('@error/valide', ['slug' => 'Connexion en cours', 'url' => $GLOBALS['BaseURL']]);

    }
}
