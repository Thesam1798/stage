<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 10/05/2018
 * Time: 16:32
 *
 * page de login utilisateur
 */
?>

<?= $renderer->render('header') ?>

<main role="main">

    <div class="album py-5 bg-light">
        <div class="container" style="text-align: center;">

            <h3>Merci de vous connecter</h3>
            <br>

            <form method="post" action="<?= $router->generateUri("home.post.index", []) ?>">
                <div class="form-group">
                    <label for="username">Nom d'utilisateur</label>
                    <input name="username" type="text" class="form-control" id="username"
                           placeholder="Entrez votre nom d'utilisateur" autofocus required>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input name="password" type="password" class="form-control" id="password"
                           placeholder="Entrez votre mot de passe" autofocus required>
                </div>
                <button type="submit" class="btn btn-success">connexion</button>
            </form>

        </div>
    </div>

</main>

<?= $renderer->render('footer') ?>

