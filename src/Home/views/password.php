<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 05/06/18
 * Time: 14:30
 *
 * page de modification de mot de passe
 */
?>

<?= $renderer->render('header') ?>

<main role="main">

    <div class="album py-5 bg-light">
        <div class="container" style="text-align: center;">

            <h3>Merci de changer votre mot de passe</h3>
            <br>

            <form method="post" action="<?= $router->generateUri("home.post.password", ['id' => $id]) ?>">
                <div class="form-group">
                    <label for="username">Nom d'utilisateur</label>
                    <input name="username" type="text" class="form-control" id="username"
                           placeholder="Entrez votre nom d'utilisateur" autofocus required>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input name="password" type="password" class="form-control" id="password"
                           placeholder="Entrez votre mot de passe" autofocus required>
                </div>
                <div class="form-group">
                    <label for="password_verif">Verification mot de passe</label>
                    <input name="password_verif" type="password" class="form-control" id="password_verif"
                           placeholder="Entrez à nouveau votre mot de passe" autofocus required>
                </div>
                <button type="submit" class="btn btn-success">Valider</button>
            </form>

        </div>
    </div>

</main>

<?= $renderer->render('footer') ?>
