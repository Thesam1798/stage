<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 21/01/2018
 * Time: 11:42
 */

namespace App\Home;

use Framework\Renderer;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class HomeModule
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * ErrorModule constructor.
     * @param Router $router
     * @param Renderer $renderer
     */
    public function __construct(Router $router, Renderer $renderer)
    {
        // Ajoute le render et le router dans des variable pour les envoyer a des sub Controller
        $this->renderer = $renderer;
        $this->router = $router;


        // Apelle d'un sub controller
        $controller = new HomeController($this->renderer,$this->router);


        // Ajout du dossier de rendue pour ce module
        $this->renderer->addPath('home', __DIR__ . DIRECTORY_SEPARATOR . 'views');


        // liste des url compatible
        // Methode(URL , [ ClassController , 'NameFunction'], 'NameRoute');
        $router->get($GLOBALS['BaseURL'].'', [$controller, 'Get_index'], 'home.get.index');

        $router->post($GLOBALS['BaseURL'].'', [$controller, 'Post_index'], 'home.post.index');
        $router->post($GLOBALS['BaseURL'].'password/{id:[0-9]+}', [$controller, 'Post_password'], 'home.post.password');
    }
}