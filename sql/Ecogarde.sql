create sequence cdc_env_eco_planning.ecogardes_id_seq;

create sequence cdc_env_eco_planning.mission_id_seq;

create sequence cdc_env_eco_planning.ext_nuisibles_id_seq;

create sequence cdc_env_eco_planning.ext_homard_id_seq;

create sequence cdc_env_eco_planning.ext_surveillance_id_seq;

create sequence cdc_env_eco_planning.ext_inventaire_id_seq;

create sequence cdc_env_eco_planning.ext_sensibilisation_id_seq;

create table cdc_env_eco_planning.ecogardes
(
  id             serial not null,
  nom            text,
  prenom         text,
  password       varchar(70),
  password_modif boolean,
  admin          boolean,
  constraint ecogardes_id_pk
  primary key (id)
);

create table cdc_env_eco_planning.mission
(
  id             serial  not null,
  id_ecogarde    integer not null,
  binome         integer,
  date           varchar(10),
  temps          varchar(5),
  commune        text,
  lieux_dit      text,
  mission_int    boolean,
  mission_ext    boolean,
  mission_code   varchar(15),
  mission_option integer,
  remarque       text,
  binome_valide  boolean default false,
  constraint mission_id_id_ecogarde_pk
  primary key (id, id_ecogarde)
);

create table cdc_env_eco_planning.ext_nuisibles
(
  id          serial  not null,
  id_mission  integer not null,
  id_ecogarde integer not null,
  type        text,
  constraint ext_nuisibles_id_id_mission_id_ecogarde_pk
  primary key (id, id_mission, id_ecogarde)
);

create unique index ext_nuisibles_id_uindex
  on cdc_env_eco_planning.ext_nuisibles (id);

create table cdc_env_eco_planning.ext_homard
(
  id           serial  not null,
  id_mission   integer not null,
  id_ecogarde  integer not null,
  partenaire   text,
  participants text,
  age          text,
  constraint ext_homard_id_id_mission_id_ecogarde_pk
  primary key (id, id_mission, id_ecogarde)
);

create unique index ext_homard_id_uindex
  on cdc_env_eco_planning.ext_homard (id);

create table cdc_env_eco_planning.ext_surveillance
(
  id             serial  not null,
  id_mission     integer not null,
  id_ecogarde    integer not null,
  controler      integer,
  procedures     integer,
  avertissements integer,
  constraint ext_surveillance_id_id_ecogarde_id_mission_pk
  primary key (id, id_ecogarde, id_mission)
);

create unique index ext_surveillance_id_uindex
  on cdc_env_eco_planning.ext_surveillance (id);

create table cdc_env_eco_planning.ext_inventaire
(
  id          serial  not null,
  id_mission  integer not null,
  id_ecogarde integer not null,
  type        text,
  nombres     integer,
  constraint ext_inventaire_id_id_ecogarde_id_mission_pk
  primary key (id, id_ecogarde, id_mission)
);

create unique index ext_inventaire_id_uindex
  on cdc_env_eco_planning.ext_inventaire (id);

create table cdc_env_eco_planning.ext_sensibilisation
(
  id            serial  not null,
  id_mission    integer not null,
  id_ecogarde   integer not null,
  estran        integer,
  integererdit  integer,
  sensibilise   integer,
  ditrib        integer,
  avant         integer,
  maille        integer,
  pendant       integer,
  trou          integer,
  non_selective integer,
  pierres       integer,
  apres         integer,
  sous_taille   integer,
  sur_peche     integer,
  constraint ext_sensibilisation_id_id_mission_id_ecogarde_pk
  primary key (id, id_mission, id_ecogarde)
);

create unique index ext_sensibilisation_id_uindex
  on cdc_env_eco_planning.ext_sensibilisation (id);

