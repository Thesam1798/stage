<?php
// Démarrage de la session utilisateur
session_start();

$GLOBALS['BaseURL'] = "/eco_planning/";

// Démarrage de l'autoloader
require dirname(__DIR__).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';


// Importation de toutes les dépendances liées au démariage de l'application
use App\Admin\AdminModule;
use App\Error\ErrorModule;
use App\Home\HomeModule;
use App\Utilisateur\UtilisateurModule;
use Framework\App;
use Framework\Renderer;
use GuzzleHttp\Psr7\ServerRequest;
use function Http\Response\send;


// Création des rendu pour les utilisateurs
$renderer = new Renderer();


// Définition du fichier de base pour les rendus
$renderer->addPath(dirname(__DIR__).DIRECTORY_SEPARATOR.'views');


// Ajout du module de base
// Tous les module sont : /src/[NomDuModule]/[NomDuModule]Module.php
$module = [
    ErrorModule::class
];

// Vérification d'utilisateur connecté
if (isset($_SESSION['User'])) {
    array_push($module, UtilisateurModule::class);
} else {
    array_push($module, HomeModule::class);
}

// Vérification administrateur
if (isset($_SESSION['Admin'])) {
    array_push($module, AdminModule::class);
}

// Construction de l'application avec les module
$app = new App($module, [
    'renderer' => $renderer
]);


// Réponse des différent module
$response = $app->run(ServerRequest::fromGlobals());


// Renvoie de la réponse à l'utilisateur
send($response);
